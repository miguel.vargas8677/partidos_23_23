<?php
class Grupos extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->load->model('grupo');
    $this->load->model('equipo');
      $this->load->model('estadio');
  }

  public function index(){
    $data["listadoGrupos"]=$this->grupo->obtenerTodos();
    $this->load->view('header');
    $this->load->view('grupos/index',$data);
    $this->load->view('footer');
  }

  public function nuevo(){
   $data["listadoEquipos"]=$this->equipo->obtenerTodos();
  $data["listadoEstadios"]=$this->estadio->obtenerTodos();
    $this->load->view('header');
    $this->load->view('grupos/nuevo', $data);
    $this->load->view('footer');
  }


public function guardarGrupo(){
    $datosNuevoGrupo=array(
			"grupo_faseg_vm"=>$this->input->post('grupo_faseg_vm'),
				"fecha_partido_faseg_vm"=>$this->input->post('fecha_partido_faseg_vm'),
				"resultado_faseg_vm"=>$this->input->post('resultado_faseg_vm'),
        "fk_id_equ_vm"=>$this->input->post('fk_id_equ_vm'),
		    "fk_id_est_vm"=>$this->input->post('fk_id_est_vm')
    );
print_r($datosNuevoGrupo);
if($this->grupo->insertar($datosNuevoGrupo)){
$this->session->set_flashdata('confirmacion','Grupo insertado exitosamente');
}else{
    $this->session->set_flashdata('Error al insertar','verifique e intente de nuevo');
}
    redirect("grupos/index");
}
//funcion para eliminar Estudiantes
public function borrar($id_faseg_vm){
if ($this->grupo->eliminarPorId($id_faseg_vm)) {
    $this->session->set_flashdata('confirmacion','grupo eliminado exitosamente');
} else {
      $this->session->set_flashdata('error','error al Eliminar','verifique e intente de nuevo');
}
redirect("grupos/index");
}
		// funcion para editar
  public function editar($id){
		$data["listadoEquipos"]=$this->equipo->obtenerTodos();
    $data["listadoEstadios"]=$this->estadio->obtenerTodos();
    $data["grupoEditar"]=$this->grupo->obtenerPorID($id);
    $this->load->view("header");
    $this->load->view("grupos/editar",$data);
    $this->load->view("footer");
  }
  public function procesarActualizacion(){
      $id_faseg_vm=$this->input->post("id_faseg_vm");
      $datosGrupoEditado=array(
        "grupo_faseg_vm"=>$this->input->post('grupo_faseg_vm'),
          "fecha_partido_faseg_vm"=>$this->input->post('fecha_partido_faseg_vm'),
          "resultado_faseg_vm"=>$this->input->post('resultado_faseg_vm'),
          "fk_id_equ_vm"=>$this->input->post('fk_id_equ_vm'),
          "fk_id_est_vm"=>$this->input->post('fk_id_est_vm'),

	    );
  if($this->grupo->actualizar($id_faseg_vm,$datosGrupoEditado)){
    $this->session->set_flashdata('confirmacion',
  'grupo actualizado exitosamente.');
       }else{
         $this->session->set_flashdata('confirmacion',
      'Error al procesar, intente nuevamente.');
   }
  redirect('grupos/index');


  }
}//cierre de la clase(FIN)
