<?php
class Terminos extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->load->model('termino');
    $this->load->model('equipo');
      $this->load->model('estadio');
  }

  public function index(){
    $data["listadoTerminos"]=$this->termino->obtenerTodos();
    $this->load->view('header');
    $this->load->view('terminos/index',$data);
    $this->load->view('footer');
  }

  public function nuevo(){
   $data["listadoEquipos"]=$this->equipo->obtenerTodos();
  $data["listadoEstadios"]=$this->estadio->obtenerTodos();
    $this->load->view('header');
    $this->load->view('terminos/nuevo', $data);
    $this->load->view('footer');
  }


public function guardarTermino(){
    $datosNuevoTermino=array(
      "fecha_partido_fin_vm"=>$this->input->post("fecha_partido_fin_vm"),
                "resultado_fin_vm"=>$this->input->post("resultado_fin_vm"),
                    "vs_fin_vm"=>$this->input->post("vs_fin_vmo"),
                    "finalp_fin_vm"=>$this->input->post("finalp_fin_vm"),
						  "fk_id_equ_vm"=>$this->input->post('fk_id_equ_vm'),
                "fk_id_faseg_vm"=>$this->input->post("fk_id_faseg_vm"),
                "fk_id_est_vm"=>$this->input->post("fk_id_est_vm"),
    );
print_r($datosNuevoTermino);
if($this->termino->insertar($datosNuevoTermino)){
$this->session->set_flashdata('confirmacion','Equipo insertado exitosamente para la final');
}else{
    $this->session->set_flashdata('Error al insertar','verifique e intente de nuevo');
}
    redirect("terminos/index");
}
//funcion para eliminar Estudiantes
public function borrar($id_fin_vm){
if ($this->termino->eliminarPorId($id_fin_vm)) {
    $this->session->set_flashdata('confirmacion','Equipo eliminado exitosamente');
} else {
      $this->session->set_flashdata('error','error al Eliminar','verifique e intente de nuevo');
}
redirect("terminos/index");
}
		// funcion para editar
  public function editar($id){
		$data["listadoEquipos"]=$this->equipo->obtenerTodos();
    $data["listadoEstadios"]=$this->estadio->obtenerTodos();
    $data["terminoEditar"]=$this->termino->obtenerPorID($id);
    $this->load->view("header");
    $this->load->view("terminos/editar",$data);
    $this->load->view("footer");
  }
  public function procesarActualizacion(){
      $id_fin_vm=$this->input->post("id_fin_vm");
      $datosTerminoEditado=array(
        "fecha_partido_fin_vm"=>$this->input->post("fecha_partido_fin_vm"),
                  "resultado_fin_vm"=>$this->input->post("resultado_fin_vm"),
                      "vs_fin_vm"=>$this->input->post("vs_fin_vmo"),
                      "finalp_fin_vm"=>$this->input->post("finalp_fin_vm"),
  						  "fk_id_equ_vm"=>$this->input->post('fk_id_equ_vm'),
                  "fk_id_faseg_vm"=>$this->input->post("fk_id_faseg_vm"),
                  "fk_id_est_vm"=>$this->input->post("fk_id_est_vm"),

	    );
  if($this->termino->actualizar($id_fin_vm,$datosTerminoEditado)){
    $this->session->set_flashdata('confirmacion',
  'Equipo actualizado exitosamente.');
       }else{
         $this->session->set_flashdata('confirmacion',
      'Error al procesar, intente nuevamente.');
   }
  redirect('terminos/index');


  }
}//cierre de la clase(FIN)
