<?php
class Tecnicos extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->load->model('tecnico');
    $this->load->model('equipo');
  }

  public function index(){
    $data["listadoTecnicos"]=$this->tecnico->obtenerTodos();
    $this->load->view('header');
    $this->load->view('tecnicos/index',$data);
    $this->load->view('footer');
  }

  public function nuevo(){
   $data["listadoEquipos"]=$this->equipo->obtenerTodos();
    $this->load->view('header');
    $this->load->view('tecnicos/nuevo', $data);
    $this->load->view('footer');
  }


public function guardarTecnico(){
    $datosNuevoTecnico=array(
			"cedula_tec_vm"=>$this->input->post('cedula_tec_vm'),
				"apellido_tec_vm"=>$this->input->post('apellido_tec_vm'),
				"nombre_tec_vm"=>$this->input->post('nombre_tec_vm'),
				"telefono_tec_vm"=>$this->input->post('telefono_tec_vm'),
		    "edad_tec_vm"=>$this->input->post('edad_tec_vm'),
		    "email_tec_vm"=>$this->input->post('email_tec_vm'),
		    "fk_id_equ_vm"=>$this->input->post('fk_id_equ_vm'),

    );

    $this->load->library("upload"); // activando la libretria de subida de archivos
  $new_name = "foto_" . time() . "_" . rand(1, 5000); //generando  un nombre aleatorio
  $config['file_name'] = $new_name;
  $config['upload_path'] = FCPATH .'uploads/tecnicos/';// ruta de subida
  $config['allowed_types'] = 'jpeg|jpg|png'; //'pdf|docx'
  $config['max_size']  = 2*1024; //5MB
  $this->upload->initialize($config); // inicializar la configuracion
  //validando la subida de archivo
  if ($this->upload->do_upload("foto_tec_vm")) {
    // que si se subio con exito
    $dataSubida = $this->upload->data();
    $datosNuevoTecnico["foto_tec_vm"]=$dataSubida['file_name'];
  }else{
    print_r ($this->upload->display_errors());
  }
// FIN
print_r($datosNuevoTecnico);
if($this->tecnico->insertar($datosNuevoTecnico)){
$this->session->set_flashdata('confirmacion','Tecnico insertado exitosamente');
}else{
    $this->session->set_flashdata('Error al insertar','verifique e intente de nuevo');
}
    redirect("tecnicos/index");
}
//funcion para eliminar Estudiantes
public function borrar($id_tec_vm){
if ($this->tecnico->eliminarPorId($id_tec_vm)) {
    $this->session->set_flashdata('confirmacion','tecnico eliminado exitosamente');
} else {
      $this->session->set_flashdata('error','error al Eliminar','verifique e intente de nuevo');
}
redirect("tecnicos/index");
}
		// funcion para editar
  public function editar($id){
		$data["listadoEquipos"]=$this->equipo->obtenerTodos();
    $data["tecnicoEditar"]=$this->tecnico->obtenerPorID($id);
    $this->load->view("header");
    $this->load->view("tecnicos/editar",$data);
    $this->load->view("footer");
  }
  public function procesarActualizacion(){
      $id_tec_vm=$this->input->post("id_tec_vm");
      $datosTecnicoEditado=array(
				"cedula_tec_vm"=>$this->input->post('cedula_tec_vm'),
					"apellido_tec_vm"=>$this->input->post('apellido_tec_vm'),
					"nombre_tec_vm"=>$this->input->post('nombre_tec_vm'),
					"telefono_tec_vm"=>$this->input->post('telefono_tec_vm'),
			    "edad_tec_vm"=>$this->input->post('edad_tec_vm'),
			    "email_tec_vm"=>$this->input->post('email_tec_vm'),
			    "fk_id_equ_vm"=>$this->input->post('fk_id_equ_vm')
	    );

      $this->load->library("upload"); // activando la libretria de subida de archivos
    $new_name = "foto_" . time() . "_" . rand(1, 5000); //generando  un nombre aleatorio
    $config['file_name'] = $new_name;
    $config['upload_path'] = FCPATH .'uploads/tecnicos/';// ruta de subida
    $config['allowed_types'] = 'jpeg|jpg|png'; //'pdf|docx'
    $config['max_size']  = 2*1024; //5MB
    $this->upload->initialize($config); // inicializar la configuracion
    //validando la subida de archivo
    if ($this->upload->do_upload("foto_tec_vm")) {
      // que si se subio con exito
      $dataSubida = $this->upload->data();
      $datosTecnicoEditado["foto_tec_vm"]=$dataSubida['file_name'];
    }else{
      print_r ($this->upload->display_errors());
    }

  if($this->tecnico->actualizar($id_tec_vm,$datosTecnicoEditado)){
    $this->session->set_flashdata('confirmacion',
  'tecnico actualizado exitosamente.');
       }else{
         $this->session->set_flashdata('confirmacion',
      'Error al procesar, intente nuevamente.');
   }
  redirect('tecnicos/index');


  }
}//cierre de la clase(FIN)
