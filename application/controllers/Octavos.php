<?php
class Octavos extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->load->model('octavo');
    $this->load->model('equipo');
      $this->load->model('estadio');
  }

  public function index(){
    $data["listadoOctavos"]=$this->octavo->obtenerTodos();
    $this->load->view('header');
    $this->load->view('Octavos/index',$data);
    $this->load->view('footer');
  }

  public function nuevo(){
   $data["listadoEquipos"]=$this->equipo->obtenerTodos();
  $data["listadoEstadios"]=$this->estadio->obtenerTodos();
    $this->load->view('header');
    $this->load->view('octavos/nuevo', $data);
    $this->load->view('footer');
  }


public function guardarOctavo(){
    $datosNuevoOctavo=array(
      "fecha_partido_oct_vm"=>$this->input->post("fecha_partido_oct_vm"),
                "resultado_oct_vm"=>$this->input->post("resultado_oct_vm"),
                    "vs_oct_vm"=>$this->input->post("vs_oct_vmo"),
                    "finalp_oct_vm"=>$this->input->post("finalp_oct_vm"),
						  "fk_id_equ_vm"=>$this->input->post('fk_id_equ_vm'),
                "fk_id_faseg_vm"=>$this->input->post("fk_id_faseg_vm"),
                "fk_id_est_vm"=>$this->input->post("fk_id_est_vm"),
    );
print_r($datosNuevoOctavo);
if($this->octavo->insertar($datosNuevoOctavo)){
$this->session->set_flashdata('confirmacion','Equipo insertado exitosamente');
}else{
    $this->session->set_flashdata('Error al insertar','verifique e intente de nuevo');
}
    redirect("octavos/index");
}
//funcion para eliminar Estudiantes
public function borrar($id_oct_vm){
if ($this->octavo->eliminarPorId($id_oct_vm)) {
    $this->session->set_flashdata('confirmacion','Equipo eliminado exitosamente');
} else {
      $this->session->set_flashdata('error','error al Eliminar','verifique e intente de nuevo');
}
redirect("octavos/index");
}
		// funcion para editar
  public function editar($id){
		$data["listadoEquipos"]=$this->equipo->obtenerTodos();
    $data["listadoEstadios"]=$this->estadio->obtenerTodos();
    $data["octavoEditar"]=$this->octavo->obtenerPorID($id);
    $this->load->view("header");
    $this->load->view("octavos/editar",$data);
    $this->load->view("footer");
  }
  public function procesarActualizacion(){
      $id_oct_vm=$this->input->post("id_oct_vm");
      $datosOctavoEditado=array(
        "fecha_partido_oct_vm"=>$this->input->post("fecha_partido_oct_vm"),
                  "resultado_oct_vm"=>$this->input->post("resultado_oct_vm"),
                      "vs_oct_vm"=>$this->input->post("vs_oct_vmo"),
                      "finalp_oct_vm"=>$this->input->post("finalp_oct_vm"),
  						  "fk_id_equ_vm"=>$this->input->post('fk_id_equ_vm'),
                  "fk_id_faseg_vm"=>$this->input->post("fk_id_faseg_vm"),
                  "fk_id_est_vm"=>$this->input->post("fk_id_est_vm"),

	    );
  if($this->octavo->actualizar($id_oct_vm,$datosOctavoEditado)){
    $this->session->set_flashdata('confirmacion',
  'Equipo actualizado exitosamente.');
       }else{
         $this->session->set_flashdata('confirmacion',
      'Error al procesar, intente nuevamente.');
   }
  redirect('octavos/index');


  }
}//cierre de la clase(FIN)
