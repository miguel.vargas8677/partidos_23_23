<?php
class Cuartos extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->load->model('cuarto');
    $this->load->model('equipo');
      $this->load->model('estadio');
  }

  public function index(){
    $data["listadoCuartos"]=$this->cuarto->obtenerTodos();
    $this->load->view('header');
    $this->load->view('cuartos/index',$data);
    $this->load->view('footer');
  }

  public function nuevo(){
   $data["listadoEquipos"]=$this->equipo->obtenerTodos();
  $data["listadoEstadios"]=$this->estadio->obtenerTodos();
    $this->load->view('header');
    $this->load->view('cuartos/nuevo', $data);
    $this->load->view('footer');
  }


public function guardarCuarto(){
    $datosNuevoCuarto=array(
      "fecha_partido_cua_vm"=>$this->input->post("fecha_partido_cua_vm"),
                "resultado_cua_vm"=>$this->input->post("resultado_cua_vm"),
                    "vs_cua_vm"=>$this->input->post("vs_cua_vmo"),
                    "finalp_cua_vm"=>$this->input->post("finalp_cua_vm"),
						  "fk_id_equ_vm"=>$this->input->post('fk_id_equ_vm'),
                "fk_id_faseg_vm"=>$this->input->post("fk_id_faseg_vm"),
                "fk_id_est_vm"=>$this->input->post("fk_id_est_vm"),
    );
print_r($datosNuevoCuarto);
if($this->cuarto->insertar($datosNuevoCuarto)){
$this->session->set_flashdata('confirmacion','Equipo insertado exitosamente');
}else{
    $this->session->set_flashdata('Error al insertar','verifique e intente de nuevo');
}
    redirect("cuartos/index");
}
//funcion para eliminar Estudiantes
public function borrar($id_cua_vm){
if ($this->cuarto->eliminarPorId($id_cua_vm)) {
    $this->session->set_flashdata('confirmacion','Equipo eliminado exitosamente');
} else {
      $this->session->set_flashdata('error','error al Eliminar','verifique e intente de nuevo');
}
redirect("cuartos/index");
}
		// funcion para editar
  public function editar($id){
		$data["listadoEquipos"]=$this->equipo->obtenerTodos();
    $data["listadoEstadios"]=$this->estadio->obtenerTodos();
    $data["cuartoEditar"]=$this->cuarto->obtenerPorID($id);
    $this->load->view("header");
    $this->load->view("cuartos/editar",$data);
    $this->load->view("footer");
  }
  public function procesarActualizacion(){
      $id_cua_vm=$this->input->post("id_cua_vm");
      $datosCuartoEditado=array(
        "fecha_partido_cua_vm"=>$this->input->post("fecha_partido_cua_vm"),
                  "resultado_cua_vm"=>$this->input->post("resultado_cua_vm"),
                      "vs_cua_vm"=>$this->input->post("vs_cua_vmo"),
                      "finalp_cua_vm"=>$this->input->post("finalp_cua_vm"),
  						  "fk_id_equ_vm"=>$this->input->post('fk_id_equ_vm'),
                  "fk_id_faseg_vm"=>$this->input->post("fk_id_faseg_vm"),
                  "fk_id_est_vm"=>$this->input->post("fk_id_est_vm"),

	    );
  if($this->cuarto->actualizar($id_cua_vm,$datosCuartoEditado)){
    $this->session->set_flashdata('confirmacion',
  'Equipo actualizado exitosamente.');
       }else{
         $this->session->set_flashdata('confirmacion',
      'Error al procesar, intente nuevamente.');
   }
  redirect('cuartos/index');


  }
}//cierre de la clase(FIN)
