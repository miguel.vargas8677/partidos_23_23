<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Equipos extends CI_Controller {
	public function __construct(){
		parent::__construct();
		  $this->load->model("equipo");
}
//renderiza la vista de estudiantes
	public function index(){
		$data["listadoEquipos"]=$this->equipo->obtenerTodos();
		$this->load->view('header');
		$this->load->view('equipos/index',$data);
		$this->load->view('footer');
	}
//renderiza la visata nuevo de estudaiantes
public function nuevo()
{
    $this->load->view('header');
    $this->load->view('equipos/nuevo');
    $this->load->view('footer');
  }


	public function guardarEquipo(){
            $datosNuevoEquipo=array(
                "nombre_equ_vm"=>$this->input->post("nombre_equ_vm"),
								"nombre1_equ_vm"=>$this->input->post("nombre1_equ_vm"),
                "abreviatura_equ_vm"=>$this->input->post("abreviatura_equ_vm"),
								"año_fundacion_equ_vm"=>$this->input->post("año_fundacion_equ_vm")
                   );
									 //Logica de Negocio necesaria para subir la FOTOGRAFIA del estudiante

     $this->load->library("upload"); // activando la libretria de subida de archivos
   $new_name = "foto_" . time() . "_" . rand(1, 5000); //generando  un nombre aleatorio
   $config['file_name'] = $new_name;
   $config['upload_path'] = FCPATH .'uploads/equipos/';// ruta de subida
   $config['allowed_types'] = 'jpeg|jpg|png'; //'pdf|docx'
   $config['max_size']  = 2*1024; //5MB
   $this->upload->initialize($config); // inicializar la configuracion
   //validando la subida de archivo
   if ($this->upload->do_upload("foto_equ_vm")) {
     // que si se subio con exito
     $dataSubida = $this->upload->data();
     $datosNuevoEquipo["foto_equ_vm"]=$dataSubida['file_name'];
   }else{
     print_r ($this->upload->display_errors());
   }
	 print_r($datosNuevoEquipo);
	 if($this->equipo->insertar($datosNuevoEquipo)){
	 $this->session->set_flashdata('confirmacion','Equipo insertado exitosamente');
	 }else{
	     $this->session->set_flashdata('Error al insertar','verifique e intente de nuevo');
	 }
	     redirect("equipos/index");
	 }
	 //funcion para eliminar Estudiantes
	 public function borrar($id_equ_vm){
	 if ($this->equipo->eliminarPorId($id_equ_vm)) {
	     $this->session->set_flashdata('confirmacion','equipo eliminado exitosamente');
	 } else {
	       $this->session->set_flashdata('error','error al Eliminar','verifique e intente de nuevo');
	 }
	 redirect("equipos/index");
	 }
				//actalizar estudaintes
				public function editar($id)
				{
					$data["equipoEditar"]=$this->equipo->obtenerPorID($id);
				    $this->load->view('header');
				    $this->load->view('equipos/editar',$data);
				    $this->load->view('footer');
				  }

					public function procesarActualizacion(){
	            $id_equ_vm=$this->input->post("id_equ_vm");
	            $datosEquipoEditado=array(
								"nombre_equ_vm"=>$this->input->post("nombre_equ_vm"),
                "abreviatura_equ_vm"=>$this->input->post("abreviatura_equ_vm"),
								"año_fundacion_equ_vm"=>$this->input->post("año_fundacion_equ_vm")
                   );

									 $this->load->library("upload");//carga de la libreria de subida de
$nombreTemporal="foto_est_vm_".time()."_".rand(1,5000);
$config["file_name"]=$nombreTemporal;
$config["upload_path"]=APPPATH.'../uploads/jugadores/';
$config["allowed_types"]="jpeg|jpg|png";
$config["max_size"]=2*1024;//2MB
$this->upload->initialize($config);
//codigo para subir el archivo y guardar el nombre en la BDD
if($this->upload->do_upload("foto_est_vm")){
  $dataSubida=$this->upload->data();
  $datosPrecioEditado["foto_pre"]=$dataSubida["file_name"];
}


	            if($this->equipo->actualizar($id_equ_vm,$datosEquipoEditado)){
								$this->session->set_flashdata('confirmacion',
							'equipo actualizado exitosamente.');
									 }else{
										 $this->session->set_flashdata('confirmacion',
									'Error al procesar, intente nuevamente.');
							 }
							redirect('equipos/index');
}
}
