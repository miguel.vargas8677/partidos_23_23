<?php
class Jugadores extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->load->model('jugador');
    $this->load->model('equipo');
  }

  public function index(){
    $data["listadoJugadores"]=$this->jugador->obtenerTodos();
    $this->load->view('header');
    $this->load->view('jugadores/index',$data);
    $this->load->view('footer');
  }

  public function nuevo(){
   $data["listadoEquipos"]=$this->equipo->obtenerTodos();
    $this->load->view('header');
    $this->load->view('jugadores/nuevo', $data);
    $this->load->view('footer');
  }


  public function guardarJugador(){
    $datosNuevoJugador=array(
			"cedula_jug_vm"=>$this->input->post('cedula_jug_vm'),
				"apellido_jug_vm"=>$this->input->post('apellido_jug_vm'),
				"nombre_jug_vm"=>$this->input->post('nombre_jug_vm'),
				"telefono_jug_vm"=>$this->input->post('telefono_jug_vm'),
				"ciudad_jug_vm"=>$this->input->post('ciudad_jug_vm'),
		    "edad_jug_vm"=>$this->input->post('edad_jug_vm'),
		    "email_jug_vm"=>$this->input->post('email_jug_vm'),
		    "fk_id_equ_vm"=>$this->input->post('fk_id_equ_vm'),
    );


  		$this->load->library("upload"); // activando la libretria de subida de archivos
      $jugadorTemporal="foto_" . time() . "_" . rand(1, 5000); //generando  un nombre aleatorio
      $config['file_name'] = $jugadorTemporal;
      $config['upload_path'] = FCPATH .'uploads/jugadores/';// ruta de subida
      $config['allowed_types'] = 'jpg|png'; //'pdf|docx'
      $config['max_size']  = 2*1024; //5MB
      $this->upload->initialize($config); // inicializar la configuracion
  		//validando la subida de archivo
      if ($this->upload->do_upload("foto_jug_vm")) {
  			// que si se subio con exito
        $dataSubida = $this->upload->data();
        $datosNuevoJugador["foto_jug_vm"] = $dataSubida['file_name'];
      }else{
  			print_r ($this->upload->display_errors());
  		}

// FIN
print_r($datosNuevoJugador);
if($this->jugador->insertar($datosNuevoJugador)){
$this->session->set_flashdata('confirmacion','Jugador insertado exitosamente');
}else{
    $this->session->set_flashdata('Error al insertar','verifique e intente de nuevo');
}
    redirect("jugadores/index");
}
//funcion para eliminar Estudiantes
public function borrar($id_jug_vm){
if ($this->jugador->eliminarPorId($id_jug_vm)) {
    $this->session->set_flashdata('confirmacion','jugador eliminado exitosamente');
} else {
      $this->session->set_flashdata('error','error al Eliminar','verifique e intente de nuevo');
}
redirect("jugadores/index");
}
		// funcion para editar
  public function editar($id){
		$data["listadoEquipos"]=$this->equipo->obtenerTodos();
    $data["jugadorEditar"]=$this->jugador->obtenerPorID($id);
    $this->load->view("header");
    $this->load->view("jugadores/editar",$data);
    $this->load->view("footer");
  }
  public function procesarActualizacion(){
      $id_jug_vm=$this->input->post("id_jug_vm");
      $datosJugadorEditado=array(
				"cedula_jug_vm"=>$this->input->post('cedula_jug_vm'),
					"apellido_jug_vm"=>$this->input->post('apellido_jug_vm'),
					"nombre_jug_vm"=>$this->input->post('nombre_jug_vm'),
					"telefono_jug_vm"=>$this->input->post('telefono_jug_vm'),
					"ciudad_jug_vm"=>$this->input->post('ciudad_jug_vm'),
			    "edad_jug_vm"=>$this->input->post('edad_jug_vm'),
			    "email_jug_vm"=>$this->input->post('email_jug_vm'),
			    "fk_id_equ_vm"=>$this->input->post('fk_id_equ_vm'),
	    );
      $this->load->library("upload"); // activando la libretria de subida de archivos
      $jugadorTemporal="foto_" . time() . "_" . rand(1, 5000); //generando  un nombre aleatorio
      $config['file_name'] = $jugadorTemporal;
      $config['upload_path'] = FCPATH .'uploads/jugadores/';// ruta de subida
      $config['allowed_types'] = 'jpeg|jpg|png'; //'pdf|docx'
      $config['max_size']  = 2*1024; //5MB
      $this->upload->initialize($config); // inicializar la configuracion
      //validando la subida de archivo
      if ($this->upload->do_upload("foto_jug_vm")) {
        // que si se subio con exito
        $dataSubida = $this->upload->data();
        $datosJugadorEditado["foto_jug_vm"] = $dataSubida['file_name'];
      }else{
        print_r ($this->upload->display_errors());
      }

  if($this->jugador->actualizar($id_jug_vm,$datosJugadorEditado)){
    $this->session->set_flashdata('confirmacion',
  'jugador actualizado exitosamente.');
       }else{
         $this->session->set_flashdata('confirmacion',
      'Error al procesar, intente nuevamente.');
   }
  redirect('jugadores/index');


  }
}//cierre de la clase(FIN)
