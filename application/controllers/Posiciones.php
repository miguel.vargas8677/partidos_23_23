<?php
class Posiciones extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->load->model('posicion');
    $this->load->model('grupo');
  }

  public function index(){
    $data["listadoPosiciones"]=$this->posicion->obtenerTodos();
    $this->load->view('header');
    $this->load->view('posiciones/index',$data);
    $this->load->view('footer');
  }

  public function nuevo(){
   $data["listadoGrupos"]=$this->grupo->obtenerTodos();
    $this->load->view('header');
    $this->load->view('posiciones/nuevo', $data);
    $this->load->view('footer');
  }


public function guardarPosicion(){
    $datosNuevoPosicion=array(
			"partido_jugados_pos_vm"=>$this->input->post('partido_jugados_pos_vm'),
				"partido_ganados_pos_vm"=>$this->input->post('partido_ganados_pos_vm'),
				"partido_perdidos_pos_vm"=>$this->input->post('partido_perdidos_pos_vm'),
				"partido_enpatados_pos_vm"=>$this->input->post('partido_enpatados_pos_vm'),
		    "goles_favor_pos_vm"=>$this->input->post('goles_favor_pos_vm'),
		    "goles_contra_pos_vm"=>$this->input->post('goles_contra_pos_vm'),
        "goles_diferencia_pos_vm"=>$this->input->post('goles_diferencia_pos_vm'),
        "puntos_pos_vm"=>$this->input->post('puntos_pos_vm'),
		    "fk_id_faseg_vm"=>$this->input->post('fk_id_faseg_vm'),

    );

// FIN
print_r($datosNuevoPosicion);
if($this->posicion->insertar($datosNuevoPosicion)){
$this->session->set_flashdata('confirmacion','Posicion insertado exitosamente');
}else{
    $this->session->set_flashdata('Error al insertar','verifique e intente de nuevo');
}
    redirect("posiciones/index");
}
//funcion para eliminar Estudiantes
public function borrar($id_pos_vm){
if ($this->posicion->eliminarPorId($id_pos_vm)) {
    $this->session->set_flashdata('confirmacion','posicion eliminado exitosamente');
} else {
      $this->session->set_flashdata('error','error al Eliminar','verifique e intente de nuevo');
}
redirect("posiciones/index");
}
		// funcion para editar
  public function editar($id){
		$data["listadoGrupos"]=$this->grupo->obtenerTodos();
    $data["posicionEditar"]=$this->posicion->obtenerPorID($id);
    $this->load->view("header");
    $this->load->view("posiciones/editar",$data);
    $this->load->view("footer");
  }
  public function procesarActualizacion(){
      $id_pos_vm=$this->input->post("id_pos_vm");
      $datosPosicionEditado=array(
        "partido_jugados_pos_vm"=>$this->input->post('partido_jugados_pos_vm'),
  				"partido_ganados_pos_vm"=>$this->input->post('partido_ganados_pos_vm'),
  				"partido_perdidos_pos_vm"=>$this->input->post('partido_perdidos_pos_vm'),
  				"partido_enpatados_pos_vm"=>$this->input->post('partido_enpatados_pos_vm'),
  		    "goles_favor_pos_vm"=>$this->input->post('goles_favor_pos_vm'),
  		    "goles_contra_pos_vm"=>$this->input->post('goles_contra_pos_vm'),
          "goles_diferencia_pos_vm"=>$this->input->post('goles_diferencia_pos_vm'),
          "puntos_pos_vm"=>$this->input->post('puntos_pos_vm'),
  		    "fk_id_faseg_vm"=>$this->input->post('fk_id_faseg_vm'),
	    );

  if($this->posicion->actualizar($id_pos_vm,$datosPosicionEditado)){
    $this->session->set_flashdata('confirmacion',
  'posicion actualizado exitosamente.');
       }else{
         $this->session->set_flashdata('confirmacion',
      'Error al procesar, intente nuevamente.');
   }
  redirect('posiciones/index');


  }
}//cierre de la clase(FIN)
