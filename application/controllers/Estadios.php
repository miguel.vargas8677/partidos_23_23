<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estadios extends CI_Controller {
	public function __construct(){
		parent::__construct();
		  $this->load->model("estadio");
}
//renderiza la vista de estudiantes
	public function index(){
		$data["listadoEstadios"]=$this->estadio->obtenerTodos();
		$this->load->view('header');
		$this->load->view('estadios/index',$data);
		$this->load->view('footer');
	}
//renderiza la visata nuevo de estudaiantes
public function nuevo()
{
    $this->load->view('header');
    $this->load->view('estadios/nuevo');
    $this->load->view('footer');
  }


	public function guardarEstadio(){
            $datosNuevoEstadio=array(
                "nombre_est_vm"=>$this->input->post("nombre_est_vm"),
								"pais_est_vm"=>$this->input->post("pais_est_vm"),
                "ciudad_est_vm"=>$this->input->post("ciudad_est_vm"),
								"ciudad_est_vm"=>$this->input->post("ciudad_est_vm")
                   );
									 //Logica de Negocio necesaria para subir la FOTOGRAFIA del estudiante

     $this->load->library("upload"); // activando la libretria de subida de archivos
   $new_name = "foto_" . time() . "_" . rand(1, 5000); //generando  un nombre aleatorio
   $config['file_name'] = $new_name;
   $config['upload_path'] = FCPATH .'uploads/estadios/';// ruta de subida
   $config['allowed_types'] = 'jpeg|jpg|png'; //'pdf|docx'
   $config['max_size']  = 2*1024; //5MB
   $this->upload->initialize($config); // inicializar la configuracion
   //validando la subida de archivo
   if ($this->upload->do_upload("foto_est_vm")) {
     // que si se subio con exito
     $dataSubida = $this->upload->data();
     $datosNuevoEstadio["foto_est_vm"]=$dataSubida['file_name'];
   }else{
     print_r ($this->upload->display_errors());
   }
	 print_r($datosNuevoEstadio);
	 if($this->estadio->insertar($datosNuevoEstadio)){
	 $this->session->set_flashdata('confirmacion','Estadio insertado exitosamente');
	 }else{
	     $this->session->set_flashdata('Error al insertar','verifique e intente de nuevo');
	 }
	     redirect("estadios/index");
	 }
	 //funcion para eliminar Estudiantes
	 public function borrar($id_est_vm){
	 if ($this->estadio->eliminarPorId($id_est_vm)) {
	     $this->session->set_flashdata('confirmacion','estadio eliminado exitosamente');
	 } else {
	       $this->session->set_flashdata('error','error al Eliminar','verifique e intente de nuevo');
	 }
	 redirect("estadios/index");
	 }
				//actalizar estudaintes
				public function editar($id)
				{
					$data["estadioEditar"]=$this->estadio->obtenerPorID($id);
				    $this->load->view('header');
				    $this->load->view('estadios/editar',$data);
				    $this->load->view('footer');
				  }

					public function procesarActualizacion(){
	            $id_est_vm=$this->input->post("id_est_vm");
	            $datosEstadioEditado=array(
								"nombre_est_vm"=>$this->input->post("nombre_est_vm"),
								"pais_est_vm"=>$this->input->post("pais_est_vm"),
								"ciudad_est_vm"=>$this->input->post("ciudad_est_vm"),
								"ciudad_est_vm"=>$this->input->post("ciudad_est_vm")
                   );

									 $this->load->library("upload");//carga de la libreria de subida de
$nombreTemporal="foto_est_vm_".time()."_".rand(1,5000);
$config["file_name"]=$nombreTemporal;
$config["upload_path"]=APPPATH.'../uploads/jugadores/';
$config["allowed_types"]="jpeg|jpg|png";
$config["max_size"]=2*1024;//2MB
$this->upload->initialize($config);
//codigo para subir el archivo y guardar el nombre en la BDD
if($this->upload->do_upload("foto_est_vm")){
  $dataSubida=$this->upload->data();
  $datosEstadioEditado["foto_est_vm"]=$dataSubida["file_name"];
}


	            if($this->estadio->actualizar($id_est_vm,$datosEstadioEditado)){
								$this->session->set_flashdata('confirmacion',
							'estadio actualizado exitosamente.');
									 }else{
										 $this->session->set_flashdata('confirmacion',
									'Error al procesar, intente nuevamente.');
							 }
							redirect('estadios/index');
}
}
