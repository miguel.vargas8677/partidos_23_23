<?php
class Eventos extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->load->model('evento');
    $this->load->model('equipo');
      $this->load->model('estadio');
  }

  public function index(){
    $data["listadoEventos"]=$this->evento->obtenerTodos();
    $this->load->view('header');
    $this->load->view('eventos/index',$data);
    $this->load->view('footer');
  }

  public function nuevo(){
   $data["listadoEquipos"]=$this->equipo->obtenerTodos();
  $data["listadoEstadios"]=$this->estadio->obtenerTodos();
    $this->load->view('header');
    $this->load->view('eventos/nuevo', $data);
    $this->load->view('footer');
  }


public function guardarEvento(){
    $datosNuevoEvento=array(
      "fecha_partido_sem_vm"=>$this->input->post("fecha_partido_sem_vm"),
                "resultado_sem_vm"=>$this->input->post("resultado_sem_vm"),
                    "vs_sem_vm"=>$this->input->post("vs_sem_vmo"),
                    "finalp_sem_vm"=>$this->input->post("finalp_sem_vm"),
						  "fk_id_equ_vm"=>$this->input->post('fk_id_equ_vm'),
                "fk_id_faseg_vm"=>$this->input->post("fk_id_faseg_vm"),
                "fk_id_est_vm"=>$this->input->post("fk_id_est_vm"),
    );
print_r($datosNuevoEvento);
if($this->evento->insertar($datosNuevoEvento)){
$this->session->set_flashdata('confirmacion','Equipo insertado exitosamente para la semifinal');
}else{
    $this->session->set_flashdata('Error al insertar','verifique e intente de nuevo');
}
    redirect("eventos/index");
}
//funcion para eliminar Estudiantes
public function borrar($id_sem_vm ){
if ($this->evento->eliminarPorId($id_sem_vm )) {
    $this->session->set_flashdata('confirmacion','Equipo eliminado exitosamente');
} else {
      $this->session->set_flashdata('error','error al Eliminar','verifique e intente de nuevo');
}
redirect("eventos/index");
}
		// funcion para editar
  public function editar($id){
		$data["listadoEquipos"]=$this->equipo->obtenerTodos();
    $data["listadoEstadios"]=$this->estadio->obtenerTodos();
    $data["eventoEditar"]=$this->evento->obtenerPorID($id);
    $this->load->view("header");
    $this->load->view("eventos/editar",$data);
    $this->load->view("footer");
  }
  public function procesarActualizacion(){
      $id_sem_vm =$this->input->post("id_sem_vm ");
      $datosEventoEditado=array(
        "fecha_partido_oct_vm"=>$this->input->post("fecha_partido_oct_vm"),
                  "resultado_oct_vm"=>$this->input->post("resultado_oct_vm"),
                      "vs_oct_vm"=>$this->input->post("vs_oct_vmo"),
                      "finalp_oct_vm"=>$this->input->post("finalp_oct_vm"),
  						  "fk_id_equ_vm"=>$this->input->post('fk_id_equ_vm'),
                  "fk_id_faseg_vm"=>$this->input->post("fk_id_faseg_vm"),
                  "fk_id_est_vm"=>$this->input->post("fk_id_est_vm"),

	    );
  if($this->evento->actualizar($id_sem_vm ,$datosEventoEditado)){
    $this->session->set_flashdata('confirmacion',
  'Equipo actualizado exitosamente.');
       }else{
         $this->session->set_flashdata('confirmacion',
      'Error al procesar, intente nuevamente.');
   }
  redirect('eventos/index');


  }
}//cierre de la clase(FIN)
