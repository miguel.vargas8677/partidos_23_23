<?php
    class Estadio extends CI_Model
    {

      public function __construct()
      {
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("estadio",$datos);
      }
//consultar los datos de nuestra base de datos
      public function obtenerTodos(){
        $estadios=$this->db->get("estadio");
        if($estadios->num_rows()>0){
          return $estadios;//cuando SI hay estudiantes
        }else{
          return false;//cuando NO hay estudiantes
        }
      }
      //funcion para eliminar un estudiante

            public function eliminarPorId($id){
              $this->db->where("id_est_vm",$id);
              return $this->db->delete("estadio");
            }

//funcion para actualizar un estudiante
public function obtenerPorID($id){
  $this->db->where("id_est_vm",$id);
  $estadio=$this->db->get("estadio");
  if($estadio->num_rows()>0){
    return $estadio->row();//cuando SI hay estudiantes
  }else{
    return false;//cuando NO hay estudiantes
  }
}
//proceco de actualizacion
public function actualizar($id,$datos){
  $this->db->where("id_est_vm",$id);
  return $this->db->update("estadio",$datos);
}


   }//cierre de la clase



   //
 ?>
