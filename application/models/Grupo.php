<?php
    class Grupo extends CI_Model
    {

      public function __construct()
      {
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("grupo",$datos);
      }
//consultar los datos de nuestra base de datos
public function obtenerTodos(){
  $this->db->join("equipo","equipo.id_equ_vm=grupo.fk_id_equ_vm");
    $this->db->join("estadio","estadio.id_est_vm=grupo.fk_id_est_vm");
  $listadoGrupos=$this->db->get("grupo");
  if($listadoGrupos->num_rows()>0){
    return $listadoGrupos;
  }else{
    return false;
  }
}
//funcion para eliminar un estudiante

      public function eliminarPorId($id){
        $this->db->where("id_faseg_vm",$id);
        return $this->db->delete("grupo");
      }

//funcion para actualizar un estudiante
public function obtenerPorID($id){
  $this->db->where("id_faseg_vm",$id);
  $this->db->join("equipo","equipo.id_equ_vm=grupo.fk_id_equ_vm");
  $this->db->join("estadio","estadio.id_est_vm=grupo.fk_id_est_vm");
  $grupo=$this->db->get("grupo");
  if($grupo->num_rows()>0){
    return $grupo->row();//cuando SI hay estudiantes
  }else{
    return false;//cuando NO hay estudiantes
  }
}
//proceco de actualizacion
public function actualizar($id,$datos){
  $this->db->where("id_faseg_vm",$id);
  return $this->db->update("grupo",$datos);
}
   }//cierre de la clase

   //
 ?>
