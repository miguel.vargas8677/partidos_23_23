<?php
    class Jugador extends CI_Model
    {

      public function __construct()
      {
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("jugador",$datos);
      }
//consultar los datos de nuestra base de datos
public function obtenerTodos(){
  $this->db->join("equipo","equipo.id_equ_vm=jugador.fk_id_equ_vm");
  $listadoJugadores=$this->db->get("jugador");
  if($listadoJugadores->num_rows()>0){
    return $listadoJugadores;
  }else{
    return false;
  }
}
//funcion para eliminar un estudiante

      public function eliminarPorId($id){
        $this->db->where("id_jug_vm",$id);
        return $this->db->delete("jugador");
      }

//funcion para actualizar un estudiante
public function obtenerPorID($id){
  $this->db->where("id_jug_vm",$id);
  $this->db->join("equipo","equipo.id_equ_vm=jugador.fk_id_equ_vm");
  $jugador=$this->db->get("jugador");
  if($jugador->num_rows()>0){
    return $jugador->row();//cuando SI hay estudiantes
  }else{
    return false;//cuando NO hay estudiantes
  }
}
//proceco de actualizacion
public function actualizar($id,$datos){
  $this->db->where("id_jug_vm",$id);
  return $this->db->update("jugador",$datos);
}
   }//cierre de la clase

   //
 ?>
