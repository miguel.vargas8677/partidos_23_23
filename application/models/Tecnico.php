<?php
    class Tecnico extends CI_Model
    {

      public function __construct()
      {
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("tecnico",$datos);
      }
//consultar los datos de nuestra base de datos
public function obtenerTodos(){
  $this->db->join("equipo","equipo.id_equ_vm=tecnico.fk_id_equ_vm");
  $listadoTecnicos=$this->db->get("tecnico");
  if($listadoTecnicos->num_rows()>0){
    return $listadoTecnicos;
  }else{
    return false;
  }
}
//funcion para eliminar un estudiante

      public function eliminarPorId($id){
        $this->db->where("id_tec_vm",$id);
        return $this->db->delete("tecnico");
      }

//funcion para actualizar un estudiante
public function obtenerPorID($id){
  $this->db->where("id_tec_vm",$id);
  $this->db->join("equipo","equipo.id_equ_vm=tecnico.fk_id_equ_vm");
  $tecnico=$this->db->get("tecnico");
  if($tecnico->num_rows()>0){
    return $tecnico->row();//cuando SI hay estudiantes
  }else{
    return false;//cuando NO hay estudiantes
  }
}
//proceco de actualizacion
public function actualizar($id,$datos){
  $this->db->where("id_tec_vm",$id);
  return $this->db->update("tecnico",$datos);
}
   }//cierre de la clase

   //
 ?>
