<?php
    class Octavo extends CI_Model
    {

      public function __construct()
      {
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("octavo",$datos);
      }
//consultar los datos de nuestra base de datos
      public function obtenerTodos(){
        $this->db->join("equipo","equipo.id_equ_vm=octavo.fk_id_equ_vm");
          $this->db->join("estadio","estadio.id_est_vm=octavo.fk_id_est_vm");
        $octavos=$this->db->get("octavo");
        if($octavos->num_rows()>0){
          return $octavos;//cuando SI hay estudiantes
        }else{
          return false;//cuando NO hay estudiantes
        }
      }
      //funcion para eliminar un estudiante

            public function eliminarPorId($id){
              $this->db->where("id_oct_vm",$id);
              return $this->db->delete("octavo");
            }

//funcion para actualizar un estudiante
public function obtenerPorID($id){
  $this->db->where("id_oct_vm",$id);
  $this->db->join("equipo","equipo.id_equ_vm=octavo.fk_id_equ_vm");
  $this->db->join("estadio","estadio.id_est_vm=octavo.fk_id_est_vm");
  $octavo=$this->db->get("octavo");
  if($octavo->num_rows()>0){
    return $octavo->row();//cuando SI hay estudiantes
  }else{
    return false;//cuando NO hay estudiantes
  }
}
//proceco de actualizacion
public function actualizar($id,$datos){
  $this->db->where("id_oct_vm",$id);
  return $this->db->update("octavo",$datos);
}


   }//cierre de la clase



   //
 ?>
