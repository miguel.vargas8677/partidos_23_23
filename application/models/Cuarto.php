<?php
    class Cuarto extends CI_Model
    {

      public function __construct()
      {
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("cuarto",$datos);
      }
//consultar los datos de nuestra base de datos
      public function obtenerTodos(){
        $this->db->join("equipo","equipo.id_equ_vm=cuarto.fk_id_equ_vm");
          $this->db->join("estadio","estadio.id_est_vm=cuarto.fk_id_est_vm");
        $cuartos=$this->db->get("cuarto");
        if($cuartos->num_rows()>0){
          return $cuartos;//cuando SI hay estudiantes
        }else{
          return false;//cuando NO hay estudiantes
        }
      }
      //funcion para eliminar un estudiante

            public function eliminarPorId($id){
              $this->db->where("id_cua_vm",$id);
              return $this->db->delete("cuarto");
            }

//funcion para actualizar un estudiante
public function obtenerPorID($id){
  $this->db->where("id_cua_vm",$id);
  $this->db->join("equipo","equipo.id_equ_vm=cuarto.fk_id_equ_vm");
  $this->db->join("estadio","estadio.id_est_vm=cuarto.fk_id_est_vm");
  $cuarto=$this->db->get("cuarto");
  if($cuarto->num_rows()>0){
    return $cuarto->row();//cuando SI hay estudiantes
  }else{
    return false;//cuando NO hay estudiantes
  }
}
//proceco de actualizacion
public function actualizar($id,$datos){
  $this->db->where("id_oct_vm",$id);
  return $this->db->update("octavo",$datos);
}


   }//cierre de la clase



   //
 ?>
