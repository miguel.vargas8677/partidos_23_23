<?php
    class Evento extends CI_Model
    {

      public function __construct()
      {
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("semifinal",$datos);
      }
//consultar los datos de nuestra base de datos
      public function obtenerTodos(){
        $this->db->join("equipo","equipo.id_equ_vm=semifinal.fk_id_equ_vm");
          $this->db->join("estadio","estadio.id_est_vm=semifinal.fk_id_est_vm");
        $eventos=$this->db->get("semifinal");
        if($eventos->num_rows()>0){
          return $eventos;//cuando SI hay estudiantes
        }else{
          return false;//cuando NO hay estudiantes
        }
      }
      //funcion para eliminar un estudiante

            public function eliminarPorId($id){
              $this->db->where("id_sem_vm ",$id);
              return $this->db->delete("semifinal");
            }

//funcion para actualizar un estudiante
public function obtenerPorID($id){
  $this->db->where("id_sem_vm ",$id);
  $this->db->join("equipo","equipo.id_equ_vm=semifinal.fk_id_equ_vm");
  $this->db->join("estadio","estadio.id_est_vm=semifinal.fk_id_est_vm");
  $evento=$this->db->get("evento");
  if($evento->num_rows()>0){
    return $evento->row();//cuando SI hay estudiantes
  }else{
    return false;//cuando NO hay estudiantes
  }
}
//proceco de actualizacion
public function actualizar($id,$datos){
  $this->db->where("id_sem_vm ",$id);
  return $this->db->update("semifinal",$datos);
}


   }//cierre de la clase



   //
 ?>
