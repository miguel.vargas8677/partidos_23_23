<?php
    class Termino extends CI_Model
    {

      public function __construct()
      {
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("final",$datos);
      }
//consultar los datos de nuestra base de datos
      public function obtenerTodos(){
        $this->db->join("equipo","equipo.id_equ_vm=final.fk_id_equ_vm");
          $this->db->join("estadio","estadio.id_est_vm=final.fk_id_est_vm");
        $terminos=$this->db->get("final");
        if($terminos->num_rows()>0){
          return $terminos;//cuando SI hay estudiantes
        }else{
          return false;//cuando NO hay estudiantes
        }
      }
      //funcion para eliminar un estudiante

            public function eliminarPorId($id){
              $this->db->where("id_fin_vm",$id);
              return $this->db->delete("final");
            }

//funcion para actualizar un estudiante
public function obtenerPorID($id){
  $this->db->where("id_fin_vm",$id);
  $this->db->join("equipo","equipo.id_equ_vm=final.fk_id_equ_vm");
  $this->db->join("estadio","estadio.id_est_vm=final.fk_id_est_vm");
  $final=$this->db->get("final");
  if($final->num_rows()>0){
    return $final->row();//cuando SI hay estudiantes
  }else{
    return false;//cuando NO hay estudiantes
  }
}
//proceco de actualizacion
public function actualizar($id,$datos){
  $this->db->where("id_fin_vm",$id);
  return $this->db->update("final",$datos);
}


   }//cierre de la clase



   //
 ?>
