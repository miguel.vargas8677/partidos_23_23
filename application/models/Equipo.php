<?php
    class Equipo extends CI_Model
    {

      public function __construct()
      {
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("equipo",$datos);
      }
//consultar los datos de nuestra base de datos
      public function obtenerTodos(){
        $equipos=$this->db->get("equipo");
        if($equipos->num_rows()>0){
          return $equipos;//cuando SI hay estudiantes
        }else{
          return false;//cuando NO hay estudiantes
        }
      }
      //funcion para eliminar un estudiante

            public function eliminarPorId($id){
              $this->db->where("id_equ_vm",$id);
              return $this->db->delete("equipo");
            }

//funcion para actualizar un estudiante
public function obtenerPorID($id){
  $this->db->where("id_equ_vm",$id);
  $equipo=$this->db->get("equipo");
  if($equipo->num_rows()>0){
    return $equipo->row();//cuando SI hay estudiantes
  }else{
    return false;//cuando NO hay estudiantes
  }
}
//proceco de actualizacion
public function actualizar($id,$datos){
  $this->db->where("id_equ_vm",$id);
  return $this->db->update("equipo",$datos);
}


   }//cierre de la clase



   //
 ?>
