<?php
    class Posicion extends CI_Model
    {

      public function __construct()
      {
        parent::__construct();
      }
      //funcion para insertar
      public function insertar($datos){
          return $this->db->insert("posicion",$datos);
      }
//consultar los datos de nuestra base de datos
public function obtenerTodos(){
  $this->db->join("grupo","grupo.id_faseg_vm=posicion.fk_id_faseg_vm");
  $listadoPosiciones=$this->db->get("posicion");
  if($listadoPosiciones->num_rows()>0){
    return $listadoPosiciones;
  }else{
    return false;
  }
}
//funcion para eliminar un estudiante

      public function eliminarPorId($id){
        $this->db->where("id_pos_vm",$id);
        return $this->db->delete("posicion");
      }

//funcion para actualizar un estudiante
public function obtenerPorID($id){
  $this->db->where("id_pos_vm",$id);
  $this->db->join("grupo","grupo.id_faseg_vm=posicion.fk_id_faseg_vm");
  $posicion=$this->db->get("posicion");
  if($posicion->num_rows()>0){
    return $posicion->row();//cuando SI hay estudiantes
  }else{
    return false;//cuando NO hay estudiantes
  }
}
//proceco de actualizacion
public function actualizar($id,$datos){
  $this->db->where("id_pos_vm",$id);
  return $this->db->update("posicion",$datos);
}
   }//cierre de la clase

   //
 ?>
