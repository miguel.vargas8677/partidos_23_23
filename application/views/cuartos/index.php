
<legend class="text-center">

      <button type="button" class="btn btn-danger">
        <span class="glyphicon glyphicon-user"></span>
      </button>

Cuartos de final
<br>
<br>
<center>
    <a href="<?php echo site_url(); ?>/cuartos/nuevo" class="btn btn-primary">
      <i class="fa fa-user-plus"></i>
      Agregar Nuevo
    </a>
  </center>
  <br>
  <br>
</legend>

<hr>

<?php if ($listadoCuartos): ?>
  <table id="tbl_cuartos" class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th class="text-center">ID</th>
          <th class="text-center">FECHA DE PARTIDO</th>
          <th class="text-center">NOMBRE DEL EQUIPO</th>

      <th class="text-center">RESULTADO1</th>
        <th class="text-center">VS</th>
        <th class="text-center">NOMBRE DEL EQUIPO</th>
            <th class="text-center">RESULTADO2</th>
          <th class="text-center">ESTADIIO </th>
        <th class="text-center">ACCIONES</th>
    </tr>
  </thead>
  <tbody >
<?php foreach ($listadoCuartos->result()
 as $cuartoTemporal): ?>
 <tr>
   <td class="text-center">
       <?php echo $cuartoTemporal->id_cua_vm; ?>
  </td>
  <td class="text-center">
      <?php echo $cuartoTemporal->fecha_partido_cua_vm ; ?>
 </td>


<td class="text-center"><?php echo $cuartoTemporal->nombre_equ_vm; ?></td>

<td class="text-center">
    <?php echo $cuartoTemporal->resultado_cua_vm; ?>
</td>
 <td class="text-center">
     <?php echo $cuartoTemporal->vs_cua_vm ; ?>
</td>

<td class="text-center"><?php echo $cuartoTemporal->nombre1_equ_vm; ?></td>
<td class="text-center">
    <?php echo $cuartoTemporal->finalp_cua_vm; ?>
</td>
 <td class="text-center">
     <?php echo $cuartoTemporal->nombre_est_vm; ?>
</td>



<td class="text-center">
  <a href="<?php echo site_url('cuartos/editar'); ?>/<?php echo $cuartoTemporal->id_cua_vm; ?>" class="btn btn-warning">
  <i class="fa fa-pencil"></i>
  Editar
    </a>
    <a href="<?php echo site_url('cuartos/borrar'); ?>/<?php echo $cuartoTemporal->id_cua_vm; ?>" class="btn btn-danger" onclick="return confirm('¿Esta seguro de eliminar?')">
    <i class="fa fa-trash"></i>
    Eliminar
    </a>


 </tr>

<?php endforeach; ?>

  </tbody>
  </table>
<?php else: ?>
<h3><b>No existe Partidos de cuartis de final</b></h3>
<?php endif; ?>

<script type="text/javascript">
$("#tbl_cuartos").DataTable();

</script>

    <!-- Results Section Begin -->
    <section class="schedule-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 left-blog-pad">
                    <div class="schedule-text">
                        <h4 class="st-title">CATAR 2022</h4>
                        <div class="st-table">
                            <table>
                                <tbody>


                                    <tr>
                                        <td class="left-team">
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Flag_of_Germany.svg/640px-Flag_of_Germany.svg.png">
                                            <h4>Alemania</h4>
                                        </td>
                                        <td class="st-option">
                                            <div class="so-text">Estadio,Estadio 974</div>
                                            <h4>2 : 2</h4>
                                            <div class="so-text">15 Septiembre 2022</div>
                                        </td>
                                        <td class="right-team">
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/Bandera_de_Espa%C3%B1a.svg/1200px-Bandera_de_Espa%C3%B1a.svg.png">
                                            <h4>España</h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="left-team">
                                            <img src="<?php echo base_url(); ?>/assets/img/schedule/flag-7.jpg" alt="">
                                            <h4>Croacia</h4>
                                        </td>
                                        <td class="st-option">
                                            <div class="so-text">Estadio,Estadio Al Bayt</div>
                                            <h4>3 : 1</h4>
                                            <div class="so-text">15 Septiembre 2022</div>
                                        </td>
                                        <td class="right-team">
                                            <img src="<?php echo base_url(); ?>/assets/img/schedule/flag-8.jpg" alt="">
                                            <h4>Uruguay</h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="left-team">
                                            <img src="<?php echo base_url(); ?>/assets/img/schedule/flag-9.jpg" alt="">
                                            <h4>Brazil</h4>
                                        </td>
                                        <td class="st-option">
                                            <div class="so-text">Estadio,
Estadio Al Janoub</div>
                                            <h4>1 : 3</h4>
                                            <div class="so-text">15 Septiembre 2022</div>
                                        </td>
                                        <td class="right-team">
                                            <img src="https://media.istockphoto.com/id/472317387/es/vector/bandera-de-suecia.jpg?s=612x612&w=0&k=20&c=ygd2DSDs7z6Jc9aaoDvP1Mgtg8p65OpAXBaq6Rlll4o=">
                                            <h4>Suezia</h4>
                                        </td>
                                    </tr>
                                    <tr>
                                  
                                        <td class="left-team">
                                            <img src="<?php echo base_url(); ?>/assets/img/schedule/flag-14.jpg" alt="">
                                            <h4>Argentina</h4>
                                        </td>
                                        <td class="st-option">
                                            <div class="so-text">Estadio, Estadio Ahmad Bin Ali</div>
                                            <h4>0 : 0</h4>
                                            <div class="so-text">15 Septiembre 2022</div>
                                        </td>
                                        <td class="right-team">
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Bandera_de_M%C3%A9xico_%281914-1920%29.svg/1200px-Bandera_de_M%C3%A9xico_%281914-1920%29.svg.png">
                                            <h4>Mexico</h4>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </section>
