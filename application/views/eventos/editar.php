<br>
<br>
<legend class="text-center">
<i class= "glyphicon glyphicon-plus"></i>Editar Partidos Ocatvos de Fanal
</legend>

<form id="frm_nuevo_evento" class=""
enctype="multipart/form-data"
action="<?php echo site_url(); ?>/eventos/procesarActualizacion"
 method="post">
<br>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">FECHA DEL PARTIDO:</label>
  </div>
  <div class="col-md-7">
    <input type="date" id="fecha_partido_sem_vm" name="fecha_partido_sem_vm" value="<?php echo $eventoEditar->fecha_partido_sem_vm; ?>"
    class="form-control" placeholder="Ingrese la fecha" >
  </div>
</div>
<br>
<div class = "form-group">
  <div class="col-md-4 text-right">
    <center><input type="hidden"name="id_sem_vm" id=id_sem_vm value="<?php echo $eventoEditar->id_sem_vm; ?>"></center>
    <br>
  <label for="">NOMBRE DEL EQUIPO 1:</label>
    </div>
  <div class="col-sm-7">

<select class="form-control" name="fk_id_equ_vm" id="fk_id_equ_vm" required>
  <option value="">----Seleccione un Equipo-----</option>
  <?php if ($listadoEquipos): ?>
    <?php foreach ($listadoEquipos->result() as $equipoTemporal): ?>
      <option value="<?php echo $equipoTemporal->id_equ_vm; ?>"><?php echo $equipoTemporal->nombre_equ_vm; ?></option>
    <?php endforeach; ?>
  <?php endif; ?>
</select>
</div>
</div>
<br>
<br>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">RESULTADO 1:</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="resultado_sem_vm" name="resultado_sem_vm" value="<?php echo $eventoEditar->resultado_sem_vm; ?>"
    class="form-control" placeholder="Ingrese el resultado" >
  </div>
</div>
<br>

<div class="row">
  <div class="col-md-4 text-right">
<label for="">NOMBRE DEL EQUIPO 2:</label>
  </div>
<div class="col-sm-7">

<select class="form-control" name="fk_id_equ_vm" id="fk_id_equ_vm" required>
<option value="">----Seleccione un Equipo-----</option>
<?php if ($listadoEquipos): ?>
  <?php foreach ($listadoEquipos->result() as $equipoTemporal): ?>
    <option value="<?php echo $equipoTemporal->id_equ_vm; ?>"><?php echo $equipoTemporal->nombre1_equ_vm; ?></option>
  <?php endforeach; ?>
<?php endif; ?>
</select>
</div>
</div>
<br>

<div class="row">
  <div class="col-md-4 text-right">
    <label for="">RESULTADO 2:</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="finalp_sem_vm" name="finalp_sem_vm" value="<?php echo $eventoEditar->finalp_sem_vm; ?>"
    class="form-control" placeholder="Ingrese el resultado" >
  </div>
</div>
<br>

<div class = "form-group">
  <div class="col-md-4 text-right">
    <center><input type="hidden" name="id_est_vm" id=id_est_vm value="<?php echo $eventoEditar->id_est_vm; ?>"></center>
    <br>
  <label for="">NOMBRE DEL ESTADIO:</label>
    </div>
  <div class="col-sm-7">

<select class="form-control" name="fk_id_est_vm" id="fk_id_est_vm" required>
  <option value="">----Seleccione un Estadio-----</option>
  <?php if ($listadoEstadios): ?>
    <?php foreach ($listadoEstadios->result() as $estadioTemporal): ?>
      <option value="<?php echo $estadioTemporal->id_est_vm; ?>"><?php echo $estadioTemporal->nombre_est_vm; ?></option>
    <?php endforeach; ?>
  <?php endif; ?>
</select>
</div>
</div>
<br>
<br>
<br>
<br>
<div class="row">
  <div class="col-md-7">
     <center><button type="submit" name="button"
    class="btn btn-primary">
    <i class="glyphicon glyphicon-ok"></i>
      GUARDAR
  </button>
  <a href="<?php echo site_url('eventos/index'); ?>" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i>
CANCELAR
  </a></center>
  </div>
<br>
<br>
<br>
</div>
</form>
<!-- ...................07 -->

<script type="text/javascript">
     $("#frm_nuevo_evento").validate({
       rules:{
         fecha_partido_sem_vm:{
           required:true,
           minlength:3

       },
         fk_id_equ_vm:{
           required:true,
         },
         resultado_sem_vm:{
           required:true,
           minlength:3


         },
        finalp_sem_vm:{
           required:true,
           minlength:3


         },
         nombre1_equ_vm:{
            required:true,
            minlength:3


          },
         fk_id_est_vm:{
           required:true,

         }
       },
       messages:{
         fecha_partido_sem_vm:{
            required:"Ingrese el la fecha",
         },
         fk_id_equ_vm:{
           required:"Ingrese del primer equipo",
         },
         resultado_sem_vm:{
            required:"Ingrese el primer resultado",
             minlength:"resultado incorrecto"
         },
         fk_id_est_vm:{
           required:"Ingrese el estadio",
         },
         finalp_sem_vm:{
            required:"Ingrese el segundo resultado",
             minlength:"resultado incorrecto"
         },
         cedula_jug_vm:{
            required:"Ingrese la cedula",
            minlength:"cedula incorrecta",
            maxlength:"cedula incorrecta",
            digits:"este campo solo acepta numeros"
         },
         apellido_jug_vm:{
            required:"Ingrese el Apellido",
             minlength:"Apellido incorrecto"
         },
         nombre_jug_vm:{
            required:"Ingrese el Nombre",
            minlength:"Nombre incorrecto"
         },
         telefono_jug_vm:{
         required:"Ingrese el numero de telefono",
         minlength:"Telefono incorrecto",
         maxlength:"Telefono incorrecto",
         digits:"Este campo solo acepta números"
         },
         ciudad_jug_vm:{
         required:"Ingrese la ciudad",
         minlength:"ciudad incorrecto"
         },
         edad_jug_vm:{
         required:"Ingrese la edad",
         },
         email_jug_vm:{
           required:"Ingrese el email",
           minlength:"email incorrecto"
         },
         foto_jug_vm:{
           required:"Ingrese la foto",
         }
       }

     });


</script>
