
<legend class="text-center">

      <button type="button" class="btn btn-danger">
        <span class="glyphicon glyphicon-user"></span>
      </button>

Semifinales
<br>
<br>
<center>
    <a href="<?php echo site_url(); ?>/eventos/nuevo" class="btn btn-primary">
      <i class="fa fa-user-plus"></i>
      Agregar Nuevo
    </a>
  </center>
  <br>
  <br>
</legend>

<hr>

<?php if ($listadoEventos): ?>
  <table id="tbl_eventos" class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th class="text-center">ID</th>
          <th class="text-center">FECHA DE PARTIDO</th>
          <th class="text-center">NOMBRE DEL EQUIPO</th>

      <th class="text-center">RESULTADO1</th>
        <th class="text-center">VS</th>
        <th class="text-center">NOMBRE DEL EQUIPO</th>
            <th class="text-center">RESULTADO2</th>
          <th class="text-center">ESTADIIO </th>
        <th class="text-center">ACCIONES</th>
    </tr>
  </thead>
  <tbody >
<?php foreach ($listadoEventos->result()
 as $eventoTemporal): ?>
 <tr>
   <td class="text-center">
       <?php echo $eventoTemporal->id_sem_vm; ?>
  </td>
  <td class="text-center">
      <?php echo $eventoTemporal->fecha_partido_sem_vm ; ?>
 </td>


<td class="text-center"><?php echo $eventoTemporal->nombre_equ_vm; ?></td>

<td class="text-center">
    <?php echo $eventoTemporal->resultado_sem_vm; ?>
</td>
 <td class="text-center">
     <?php echo $eventoTemporal->vs_sem_vm ; ?>
</td>

<td class="text-center"><?php echo $eventoTemporal->nombre1_equ_vm; ?></td>
<td class="text-center">
    <?php echo $eventoTemporal->finalp_sem_vm; ?>
</td>
 <td class="text-center">
     <?php echo $eventoTemporal->nombre_est_vm; ?>
</td>



<td class="text-center">
  <a href="<?php echo site_url('eventos/editar'); ?>/<?php echo $eventoTemporal->id_sem_vm ; ?>" class="btn btn-warning">
  <i class="fa fa-pencil"></i>
  Editar
    </a>
    <a href="<?php echo site_url('eventos/borrar'); ?>/<?php echo $eventoTemporal->id_sem_vm ; ?>" class="btn btn-danger" onclick="return confirm('¿Esta seguro de eliminar?')">
    <i class="fa fa-trash"></i>
    Eliminar
    </a>


 </tr>

<?php endforeach; ?>

  </tbody>
  </table>
<?php else: ?>
<h3><b>No existe Partidos de Semifinales</b></h3>
<?php endif; ?>

<script type="text/javascript">
$("#tbl_eventos").DataTable();

</script>
