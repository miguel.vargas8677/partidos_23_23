

<legend class="text-center">
<i class= "glyphicon glyphicon-plus"></i>Nuevos Jugadores
</legend>

<form id="frm_nuevo_jugador" class=""
enctype="multipart/form-data"
action="<?php echo site_url(); ?>/jugadores/guardarJugador" method="post">
<br>
<div class = "form-group">
  <div class="col-md-4 text-right">
  <label for="">NOMBRE DEL EQUIPO:</label>
    </div>
  <div class="col-sm-7">
<select class="form-control" name="fk_id_equ_vm" id="fk_id_equ_vm" >
<option value="">----Seleccione un Equipo-----</option>
<?php if ($listadoEquipos): ?>
<?php foreach ($listadoEquipos->result() as $equipoTemporal): ?>
  <option value="<?php echo $equipoTemporal->id_equ_vm; ?>"><?php echo $equipoTemporal->nombre_equ_vm; ?></option>
<?php endforeach; ?>
<?php endif; ?>
</select>
</div>
</div>
<br>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">CEDULA:</label>
  </div>
  <div class="col-md-7">
    <input type="number" id="cedula_jug_vm" name="cedula_jug_vm" value=""
    class="form-control" placeholder="Ingrese el numero de cedula" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">APELLIDO:</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="apellido_jug_vm" name="apellido_jug_vm" value=""
    class="form-control" placeholder="Ingrese el apellido" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">NOMBRE:</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="nombre_jug_vm" name="nombre_jug_vm" value=""
    class="form-control" placeholder="Ingrese el nombre" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">TELEFONO:</label>
  </div>
  <div class="col-md-7">
    <input type="number" id="telefono_jug_vm" name="telefono_jug_vm" value=""
    class="form-control" placeholder="Ingrese el numero de telefono" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">CIUDAD:</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="ciudad_jug_vm" name="ciudad_jug_vm" value=""
    class="form-control" placeholder="Ingrese la direccion" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">EDAD:</label>
  </div>
  <div class="col-md-7">
    <input type="number" id="edad_jug_vm" name="edad_jug_vm" value=""
    class="form-control" placeholder="Seleccione la edad" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">EMAIL:</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="email_jug_vm" name="email_jug_vm" value=""
    class="form-control" placeholder="Ingrese el email" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">FOTO:</label>
  </div>
  <div class="col-md-7">
    <input type="file" id="foto_jug_vm" name="foto_jug_vm" value=""  accept="image/*" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-7">
     <center><button type="submit" name="button"
    class="btn btn-primary">
    <i class="glyphicon glyphicon-ok"></i>
      GUARDAR
  </button>
  <a href="<?php echo site_url('jugadores/index'); ?>" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i>
CANCELAR
  </a></center>
  </div>

</div>
</form>
<!-- ...................07 -->
<script type="text/javascript">
     $("#frm_nuevo_jugador").validate({
       rules:{
         fk_id_equ_vm:{
           required:true,
         },
         cedula_jug_vm:{
           required:true,
           minlength:10,
           maxlength:10,
           digits:true
         },
         apellido_jug_vm:{
           required:true,
           minlength:3


         },
         nombre_jug_vm:{
           required:true,
           minlength:3


         },
         telefono_jug_vm:{
           required:true,
           minlength:10,
           maxlength:10,
           digits:true

         },
         ciudad_jug_vm:{
           required:true,
           minlength:3

         },
         edad_jug_vm:{
           required:true,
         },
         email_jug_vm:{
           required:true,
           minlength:3

         },
         foto_jug_vm:{
           required:true,
         }
       },
       messages:{
         fk_id_equ_vm:{
           required:"Ingrese el dato",
         },
         cedula_jug_vm:{
            required:"Ingrese la cedula",
            minlength:"cedula incorrecta",
            maxlength:"cedula incorrecta",
            digits:"este campo solo acepta numeros"
         },
         apellido_jug_vm:{
            required:"Ingrese el Apellido",
             minlength:"Apellido incorrecto"
         },
         nombre_jug_vm:{
            required:"Ingrese el Nombre",
            minlength:"Nombre incorrecto"
         },
         telefono_jug_vm:{
         required:"Ingrese el numero de telefono",
         minlength:"Telefono incorrecto",
         maxlength:"Telefono incorrecto",
         digits:"Este campo solo acepta números"
         },
         ciudad_jug_vm:{
         required:"Ingrese la ciudad",
         minlength:"ciudad incorrecto"
         },
         edad_jug_vm:{
         required:"Ingrese la edad",
         },
         email_jug_vm:{
           required:"Ingrese el email",
           minlength:"email incorrecto"
         },
         foto_jug_vm:{
           required:"Ingrese la foto",
         }
       }

     });


</script>
