<legend class="text-center">
<i class= "glyphicon glyphicon-user"></i>Gestion de Jugadores
</legend>
<hr>
<center>
  <a href="<?php echo site_url('jugadores/nuevo'); ?>"
    class="btn btn-success"
    <i class="glyphicon glyphicon-plus glyphicon-circle"></i>
    AGREGAR JUGADORES
  </a>
</center>
<br>
<?php if ($listadoJugadores): ?>
  <table id="tbl-jugadores" class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE DEL EQUIPO</th>
        <th class="text-center">CEDULA</th>
        <th class="text-center">APELLIDO</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">TELEFONO</th>
        <th class="text-center">CIUDAD</th>
        <th class="text-center">EDAD</th>
        <th class="text-center">EMAIL</th>

        <th class="text-center">FOTO</th>
        <th class="text-center">ACCIONES</th>

      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoJugadores->result() as $jugadorTemporal): ?>
<tr>
  <td class="text-center"><?php echo $jugadorTemporal->id_jug_vm; ?></td>
  <td class="text-center"><?php echo $jugadorTemporal->nombre_equ_vm; ?></td>
  <td class="text-center"><?php echo $jugadorTemporal->cedula_jug_vm; ?></td>
  <td class="text-center"><?php echo $jugadorTemporal->apellido_jug_vm; ?></td>
  <td class="text-center"><?php echo $jugadorTemporal->nombre_jug_vm; ?></td>
  <td class="text-center"><?php echo $jugadorTemporal->telefono_jug_vm; ?></td>
  <td class="text-center"><?php echo $jugadorTemporal->ciudad_jug_vm; ?></td>
  <td class="text-center"><?php echo $jugadorTemporal->edad_jug_vm; ?></td>
  <td class="text-center"><?php echo $jugadorTemporal->email_jug_vm; ?></td>


   <td class="text-center">
                                     <?php if ($jugadorTemporal->foto_jug_vm!=""): ?>
                                       <a href="<?php echo base_url(); ?>/uploads/jugadores/<?php echo $jugadorTemporal->foto_jug_vm; ?>"
                                         target="_blank">
                                       <img class="img-circle" src="<?php echo base_url(); ?>/uploads/jugadores/<?php echo $jugadorTemporal->foto_jug_vm; ?>"
                                       height="80px"
                                       width="100px"
                                       alt="">
                                       </a>
                                     <?php else: ?>
                                       N/A
                                     <?php endif; ?>
                                   </td>


       <td class="text-center">
         <a href="<?php echo site_url('jugadores/editar'); ?>/<?php echo $jugadorTemporal->id_jug_vm; ?>" class="btn btn-warning">
           <i class="fa fa-pencil"></i>
           Editar
        </a>
       <a href="<?php echo site_url('jugadores/borrar'); ?>/<?php echo $jugadorTemporal->id_jug_vm; ?>" class="btn btn-danger" onclick="return confirm('¿Esta seguro de eliminar?')">
           <i class="fa fa-trash"></i>
           Eliminar
           </a>
          </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php else: ?>
  <h3><br>No existen jugadores</br></h3>
<?php endif; ?>
<script type="text/javascript">
$("#tbl-jugadores").DataTable();

</script>
