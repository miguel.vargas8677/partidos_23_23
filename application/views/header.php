<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Specer Template">
    <meta name="keywords" content="Specer, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>MUNDIAL QATAR</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/css/style.css" type="text/css">

    <!-- Latest compiled and minified CSS -->
    <!-- Latest compiled and minified CSS -->
    <!-- Compiled and minified Bootstrap CSS -->
    <script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <!-- cdn de boostrap -->
    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<!-- jqery validate -->

<!-- importacion de datatables -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js">
</script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!-- CSS only -->


<!-- sweetalet mensajes de confirmacion -->
<!-- sweetalet mensajes de confirmacion -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.6.15/sweetalert2.css" integrity="sha512-JzSVRb7c802/njMbV97pjo1wuJAE/6v9CvthGTDxiaZij/TFpPQmQPTcdXyUVucsvLtJBT6YwRb5LhVxX3pQHQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.6.15/sweetalert2.js" integrity="sha512-9V+5wAdU/RmYn1TP+MbEp5Qy9sCDYmvD2/Ub8sZAoWE2o6QTLsKx/gigfub/DlOKAByfhfxG5VKSXtDlWTcBWQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!-- Importacion de Jquery Validation -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/jquery.validate.min.js" integrity="sha512-FOhq9HThdn7ltbK8abmGn60A/EMtEzIzv1rvuh+DqzJtSGq8BRdEN0U+j0iKEIffiw/yEtVuladk6rsG4X6Uqg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/additional-methods.min.js" integrity="sha512-XJiEiB5jruAcBaVcXyaXtApKjtNie4aCBZ5nnFDIEFrhGIAvitoqQD6xd9ayp5mLODaCeaXfqQMeVs1ZfhKjRQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.4/localization/messages_es_AR.min.js" integrity="sha512-HHnzo0ssMRoNapdoTaORwzLpemBFMsg7GA8fr0d9xS1rEXKHazYMTUAUka2abGFCfsdXgZPVVyv3LCkXP1Fhsg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>


<script type="text/javascript">
    jQuery.validator.addMethod("letras", function(value, element) {
        //return this.optional(element) || /^[a-z]+$/i.test(value);
        return this.optional(element) || /^[A-Za-zÁÉÍÑÓÚáé íñó]*$/.test(value);
    }, "Este campo solo acepta letras");
</script>
<!-- importacion de fileinput.js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.5.0/js/fileinput.min.js" integrity="sha512-C9i+UD9eIMt4Ufev7lkMzz1r7OV8hbAoklKepJW0X6nwu8+ZNV9lXceWAx7pU1RmksTb1VmaLDaopCsJFWSsKQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.5.0/css/fileinput.min.css" integrity="sha512-XHMymTWTeqMm/7VZghZ2qYTdoJyQxdsauxI4dTaBLJa8d1yKC/wxUXh6lB41Mqj88cPKdr1cn10SCemyLcK76A==" crossorigin="anonymous" referrerpolicy="no-referrer" />

  </head>


  <body>


      <!-- Offcanvas Menu Section Begin -->
      <div class="offcanvas-menu-overlay"></div>
      <div class="offcanvas-menu-wrapper">
          <div class="canvas-close">
              <i class="fa fa-close"></i>
          </div>
          <div class="search-btn search-switch">
              <i class="fa fa-search"></i>
          </div>
          <div class="header__top--canvas">
              <div class="ht-info">
                  <ul>
                      <li>20:00 - May 19, 2012</li>
                      <li><a href="#">Iniciar Secion</a></li>
                      <li><a href="#">Contactanos</a></li>
                  </ul>
              </div>
              <div class="ht-links">
                  <a href="#"><i class="fa fa-facebook"></i></a>
                  <a href="#"><i class="fa fa-vimeo"></i></a>
                  <a href="#"><i class="fa fa-twitter"></i></a>
                  <a href="#"><i class="fa fa-google-plus"></i></a>
                  <a href="#"><i class="fa fa-instagram"></i></a>
              </div>
          </div>

          <ul class="main-menu mobile-menu">
              <li class="active"><a href="./index.html">Home</a></li>
              <li><a href="./club.html">Club</a></li>
              <li><a href="./schedule.html">Schedule</a></li>
              <li><a href="./result.html">Results</a></li>
              <li><a href="#">Sport</a></li>
              <li><a href="#">Pages</a>
                  <ul class="dropdown">
                      <li><a href="./blog.html">Blog</a></li>
                      <li><a href="./blog-details.html">Blog Details</a></li>
                      <li><a href="#">Schedule</a></li>
                      <li><a href="#">Results</a></li>
                  </ul>
              </li>
              <li><a href="./contact.html">Contact Us</a></li>
          </ul>
          <div id="mobile-menu-wrap"></div>
      </div>
      <!-- Offcanvas Menu Section End -->

      <!-- Header Section Begin -->
      <header class="header-section">
          <div class="header__top">
              <div class="container">
                  <div class="row">
                      <div class="col-lg-6">
                          <div class="ht-info">
                              <ul>
                                  <li>20:00 - May 19, 2019</li>
                                  <li><a href="#">Sign in</a></li>
                                  <li><a href="#">Contact</a></li>
                              </ul>
                          </div>
                      </div>
                      <div class="col-lg-6">
                          <div class="ht-links">
                              <a href="#"><i class="fa fa-facebook"></i></a>
                              <a href="#"><i class="fa fa-vimeo"></i></a>
                              <a href="#"><i class="fa fa-twitter"></i></a>
                              <a href="#"><i class="fa fa-google-plus"></i></a>
                              <a href="#"><i class="fa fa-instagram"></i></a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="header__nav">
              <div class="container">
                  <div class="row">
                      <div class="col-lg-2">
                          <div class="logo">
                              <a href="./index.html"><img src="img/logo.png" alt=""></a>
                          </div>
                      </div>
                      <div class="col-lg-12">
                          <div class="nav-menu">
                              <ul class="main-menu">

                                          <li class="nav-item">
                                            <a class="nav-link" href="<?php echo site_url(); ?>">
                                              <i class="typcn typcn-device-desktop menu-icon"></i>
                                              <span class="menu-title">Inicio</span>

                                            </a>
                                          </li>

                                  <li class="nav-item">
            <a class="nav-link"
            href="<?php echo site_url(); ?>/equipos/index">
              <i class="typcn typcn-device-desktop menu-icon"></i>
              <span class="menu-title">Equipos</span>
            </a>
          </li>


          <li class="nav-item">
            <a class="nav-link"
            href="<?php echo site_url(); ?>/jugadores/index">
              <i class="typcn typcn-device-desktop menu-icon"></i>
              <span class="menu-title">Jugadores</span>
            </a>
          </li>


          <li class="nav-item">
            <a class="nav-link"
            href="<?php echo site_url(); ?>/tecnicos/index">
              <i class="typcn typcn-device-desktop menu-icon"></i>
              <span class="menu-title">Tecnicos</span>
            </a>
          </li>


          <li class="nav-item">
            <a class="nav-link"
            href="<?php echo site_url(); ?>/estadios/index">
              <i class="typcn typcn-device-desktop menu-icon"></i>
              <span class="menu-title">Estadios</span>
            </a>
          </li>


                    <li class="nav-item">
                      <a class="nav-link"
                      href="<?php echo site_url(); ?>/grupos/index">
                        <i class="typcn typcn-device-desktop menu-icon"></i>
                        <span class="menu-title">Grupos</span>
                      </a>
                    </li>

                    <li class="nav-item">
                      <a class="nav-link"
                      href="<?php echo site_url(); ?>/posiciones/index">
                        <i class="typcn typcn-device-desktop menu-icon"></i>
                        <span class="menu-title">Posiciones</span>
                      </a>
                    </li>


                                  <li><a href="#">Fase final</a>
                                      <ul class="dropdown">
                                          <li><a href="<?php echo site_url(); ?>/octavos/index">Octavos</a></li>
                                          <li><a href="<?php echo site_url(); ?>/cuartos/index">Cuartos de Final</a></li>
                                          <li><a href="<?php echo site_url(); ?>/eventos/index">Semifinal</a></li>
                                          <li><a href="<?php echo site_url(); ?>/terminos/index">Final</a></li>
                                      </ul>
                                  </li>

                        

                              <div class="nm-right search-switch">
                                  <i class="fa fa-search"></i>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="canvas-open">
                      <i class="fa fa-bars"></i>
                  </div>
              </div>
          </div>
      </header>
      <!-- Header End -->
