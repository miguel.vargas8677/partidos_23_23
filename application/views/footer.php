

    <!-- Footer Section Begin -->
    <footer class="footer-section set-bg" data-setbg="<?php echo base_url('assets/'); ?>img/footer-bg.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="fs-logo">
                        <div class="logo">
                            <a href="./index.html"><img src="<?php echo base_url('assets/'); ?>img/logo.png" alt=""></a>
                        </div>
                        <ul>
                            <li><i class="fa fa-envelope"></i> Info.mundialistas@gmail.com</li>
                            <li><i class="fa fa-copy"></i> +(12) 345 6789 10</li>
                            <li><i class="fa fa-thumb-tack"></i> Catar 2022-2023</li>
                        </ul>
                        <div class="fs-social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-tumblr"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 offset-lg-1">
                    <div class="fs-widget">
                        <h4>Top Club</h4>
                        <ul class="fw-links">
                            <li><a href="#">Brazil</a></li>
                            <li><a href="#">Belgica</a></li>
                            <li><a href="#">Argentina</a></li>
                            <li><a href="#">Franica</a></li>
                            <li><a href="#">Inglaterra</a></li>
                            <li><a href="#">Italia</a></li>
                        </ul>
                        <ul class="fw-links">
                            <li><a href="#">España</a></li>
                            <li><a href="#">Portugal</a></li>
                            <li><a href="#">Uruguay</a></li>
                            <li><a href="#">Ecuador</a></li>
                            <li><a href="#">Mexico</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="fs-widget">
                        <h4>Noticias recientes</h4>

                        <div class="fw-item">
                            <h5><a href="#">Los Mejores equipos ya estan en la fase final del mundial "Catar"</a></h5>
                            <ul>
                                <li><i class="fa fa-calendar"></i> May 19, 2019</li>
                                <li><i class="fa fa-edit"></i> 3 Comment</li>
                            </ul>
                        </div>
                        <div class="fw-item">
                            <h5><a href="#">Argentina vs Francia: estan nuevamente en una final 2022</a></h5>
                            <ul>
                                <li><i class="fa fa-calendar"></i> May 19, 2019</li>
                                <li><i class="fa fa-edit"></i> 3 Comment</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="copyright-option">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="co-text"><p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
  Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
  <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p></div>
                        <div class="co-widget">
                            <ul>
                                <li><a href="#">Copyright notification</a></li>
                                <li><a href="#">Terms of Use</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Search model Begin -->
    <div class="search-model">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-switch"><i class="fa fa-close"></i></div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Search here.....">
            </form>
        </div>
    </div>
    <!-- Search model end -->

    <!-- Js Plugins -->
    <script src="<?php echo base_url('assets/'); ?>js/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>js/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>js/jquery.slicknav.js"></script>
    <script src="<?php echo base_url('assets/'); ?>js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url('assets/'); ?>js/main.js"></script>



        <style>

        label.error{
            border:1px solid white;
            color:white;
            background-color:#E15B69;
            padding:5px;
            padding-left:15px;
            padding-right:15px;
            font-size:12px;
            opacity: 0;
              /visibility: hidden;/
              /position: absolute;/
              left: 0px;
              transform: translate(0, 10px);
              /background-color: white;/
              /padding: 1.5rem;/
              box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.26);
              width: auto;
              margin-top:30px !important;


              z-index: 10;
              opacity: 1;
              visibility: visible;
              transform: translate(0, -20px);
              transition: all 0.5s cubic-bezier(0.75, -0.02, 0.2, 0.97);
              border-radius:10px;
              width:100%;

        }

        input.error{
            border:1px solid #E15B69;
        }


        select.error{
            border:1px solid #E15B69;
        }

        label.error:before{
             position: absolute;
              z-index: -1;
              content: "";
              right: calc(90% - 10px);
              top: -8px;
              border-style: solid;
              border-width: 0 10px 10px 10px;
              border-color: transparent transparent #E15B69 transparent;
              transition-duration: 0.3s;
              transition-property: transform;
        }


    </style>

    <?php if ($this->session->flashdata('confirmacion')):?>
<script type="text/javascript">
$(document).ready(function(){
  Swal.fire(
  'CONFIRMACION', //titulo
  '<?php echo $this->session->flashdata('confirmacion'); ?>',
  'success'//TIPO
  )
});
  </script>
  <?php $this->session->flashdata('confirmacion',''); ?>

    <?php endif; ?>


        <?php if ($this->session->flashdata('error')):?>
    <script type="text/javascript">
    $(document).ready(function(){
      Swal.fire(
      'error', //titulo
      '<?php echo $this->session->flashdata('error'); ?>',
      'success'//TIPO
      )
    });
      </script>

        <?php endif; ?>

  </body>
</html>

</body>

</html>
