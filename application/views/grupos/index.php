<legend class="text-center">
<i class= "glyphicon glyphicon-user"></i>Gestion de Tecnicos
</legend>
<hr>
<center>
  <a href="<?php echo site_url('grupos/nuevo'); ?>"
    class="btn btn-success"
    <i class="glyphicon glyphicon-plus glyphicon-circle"></i>
    AGREGAR GRUPOS
  </a>
</center>
<br>
<?php if ($listadoGrupos): ?>
  <table id="tbl-grupos" class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE DEL EQUIPO</th>
        <th class="text-center">NOMBRE DEL ESTADIO</th>
        <th class="text-center">GRUPO</th>
        <th class="text-center">FECHA DE PARTIDO</th>
        <th class="text-center">RESULTADO</th>
        <th class="text-center">ACCIONES</th>

      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoGrupos->result() as $grupoTemporal): ?>
<tr>
  <td class="text-center"><?php echo $grupoTemporal->id_faseg_vm; ?></td>
    <td class="text-center"><?php echo $grupoTemporal->nombre_equ_vm; ?></td>
    <td class="text-center"><?php echo $grupoTemporal->nombre_est_vm; ?></td>
  <td class="text-center"><?php echo $grupoTemporal->grupo_faseg_vm; ?></td>
  <td class="text-center"><?php echo $grupoTemporal->fecha_partido_faseg_vm; ?></td>
  <td class="text-center"><?php echo $grupoTemporal->resultado_faseg_vm; ?></td>


<td class="text-center">
<a href="<?php echo site_url("grupos/editar"); ?>/<?php echo $grupoTemporal->id_faseg_vm; ?>" class="btn btn-warning">
<i class="glyphicon glyphicon-edit"></i>
  EDITAR
</a>
<a href="<?php echo site_url("grupos/borrar"); ?>/<?php echo $grupoTemporal->id_faseg_vm; ?>" class="btn btn-danger" onclick="return confirm('¿esta seguro de eliminar?');">
<i class="glyphicon glyphicon-trash"></i>
ELIMINAR
</a>
</tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php else: ?>
<h3><br>No existen grupos</br></h3>
<?php endif; ?>
<script type="text/javascript">
$("#tbl-grupos").DataTable();

</script>
