<br>
<br>
<legend class="text-center">
<i class= "glyphicon glyphicon-plus"></i>Nuevos Grupos
</legend>

<form id="frm_nuevo_grupo" class=""
enctype="multipart/form-data"
action="<?php echo site_url(); ?>/grupos/guardarGrupo" method="post">
<br>
<div class = "form-group">
  <div class="col-md-4 text-right">
  <label for="">NOMBRE DEL EQUIPO:</label>
    </div>
  <div class="col-sm-7">
<select class="form-control" name="fk_id_equ_vm" id="fk_id_equ_vm" >
<option value="">----Seleccione un Equipo-----</option>
<?php if ($listadoEquipos): ?>
<?php foreach ($listadoEquipos->result() as $equipoTemporal): ?>
  <option value="<?php echo $equipoTemporal->id_equ_vm; ?>"><?php echo $equipoTemporal->nombre_equ_vm; ?></option>
<?php endforeach; ?>
<?php endif; ?>
</select>
</div>
</div>
<br>
<br>
<div class = "form-group">
  <div class="col-md-4 text-right">
  <label for="">NOMBRE DEL ESTADIO:</label>
    </div>
  <div class="col-sm-7">
<select class="form-control" name="fk_id_est_vm" id="fk_id_est_vm" >
<option value="">----Seleccione un Estadio-----</option>
<?php if ($listadoEstadios): ?>
<?php foreach ($listadoEstadios->result() as $estadioTemporal): ?>
  <option value="<?php echo $estadioTemporal->id_est_vm; ?>"><?php echo $estadioTemporal->nombre_est_vm; ?></option>
<?php endforeach; ?>
<?php endif; ?>
</select>
</div>
</div>
<br>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">GRUPO:</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="grupo_faseg_vm" name="grupo_faseg_vm" value=""
    class="form-control" placeholder="Ingrese el grupo" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">FECHA DEL PARTIDO:</label>
  </div>
  <div class="col-md-7">
    <input type="date" id="fecha_partido_faseg_vm" name="fecha_partido_faseg_vm" value=""
    class="form-control" placeholder="Ingrese la fecha" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">RESULTADO:</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="resultado_faseg_vm" name="resultado_faseg_vm" value=""
    class="form-control" placeholder="Ingrese el resultado" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-7">
     <center><button type="submit" name="button"
    class="btn btn-primary">
    <i class="glyphicon glyphicon-ok"></i>
      GUARDAR
  </button>
  <a href="<?php echo site_url('grupos/index'); ?>" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i>
CANCELAR
  </a></center>
  </div>

</div>
</form>
<br>
<br>
<br>
<!-- ...................07 -->
<script type="text/javascript">
     $("#frm_nuevo_grupo").validate({
       rules:{
         fk_id_equ_vm:{
           required:true,
         },
         fk_id_est_vm:{
           required:true,
         },
         grupo_faseg_vm:{
           required:true,



         },
         fecha_partido_faseg_vm:{
           required:true,
           minlength:3
         },
         resultado_faseg_vm:{
           required:true,
           minlength:1
         }

       },
       messages:{
         fk_id_equ_vm:{
           required:"Ingrese el dato",
         },
         fk_id_equ_vm:{
           required:"Ingrese el dato",
         },

          grupo_faseg_vm:{
            required:"Ingrese el Grupo",

         },
         fecha_partido_faseg_vm:{
            required:"Ingrese la fecha de partido",
            minlength:"Fecha incorrecto"
         },
          resultado_faseg_vm:{
            required:"Ingrese el resultado",
            minlength:"Resultado incorrecto"
         }

       }

     });


</script>
