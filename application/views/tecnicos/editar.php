

<legend class="text-center">
<i class= "glyphicon glyphicon-plus"></i>Editar Tecnicos
</legend>

<form id="frm_nuevo_tecnico" class=""
enctype="multipart/form-data"
action="<?php echo site_url(); ?>/tecnicos/procesarActualizacion"
 method="post">
<br>
<div class = "form-group">
  <div class="col-md-4 text-right">
    <center><input type="hidden"name="id_tec_vm" id=id_equ_vm value="<?php echo $tecnicoEditar->id_tec_vm; ?>"></center>
    <br>
  <label for="">NOMBRE DEL EQUIPO:</label>
    </div>
  <div class="col-sm-7">

<select class="form-control" name="fk_id_equ_vm" id="fk_id_equ_vm" required>
  <option value="">----Seleccione un Equipo-----</option>
  <?php if ($listadoEquipos): ?>
    <?php foreach ($listadoEquipos->result() as $equipoTemporal): ?>
      <option value="<?php echo $equipoTemporal->id_equ_vm; ?>"><?php echo $equipoTemporal->nombre_equ_vm; ?></option>
    <?php endforeach; ?>
  <?php endif; ?>
</select>
</div>
</div>
<br>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">CEDULA:</label>
  </div>
  <div class="col-md-7">
    <input type="number" id="cedula_tec_vm" name="cedula_tec_vm" value="<?php echo $tecnicoEditar->cedula_tec_vm; ?>"
    class="form-control" placeholder="Ingrese el numero de cedula" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">APELLIDO:</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="apellido_tec_vm" name="apellido_tec_vm" value="<?php echo $tecnicoEditar->apellido_tec_vm; ?>"
    class="form-control" placeholder="Ingrese el apellido" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">NOMBRE:</label>
  </div>
  <div class="col-md-7">
    <input type="text" id="nombre_tec_vm" name="nombre_tec_vm" value="<?php echo $tecnicoEditar->nombre_tec_vm; ?>"
    class="form-control" placeholder="Ingrese el nombre" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">TELEFONO:</label>
  </div>
  <div class="col-md-7">
    <input type="number" id="telefono_tec_vm" name="telefono_tec_vm" value="<?php echo $tecnicoEditar->telefono_tec_vm; ?>"
    class="form-control" placeholder="Ingrese el numero de telefono" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">EDAD:</label>
  </div>
  <div class="col-md-7">
    <input type="number" id="edad_tec_vm" name="edad_tec_vm" value="<?php echo $tecnicoEditar->edad_tec_vm; ?>"
    class="form-control" placeholder="Seleccione la edad" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">EMAIL:</label>
  </div>
  <div class="col-md-7">
    <input type="email" id="email_tec_vm" name="email_tec_vm" value="<?php echo $tecnicoEditar->email_tec_vm; ?>"
    class="form-control" placeholder="Ingrese el email" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">FOTO:</label>
  </div>
  <div class="col-md-7">
    <input type="file" id="foto_tec_vm" name="foto_tec_vm" value="<?php echo $tecnicoEditar->foto_tec_vm; ?>"  accept="image/*" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-7">
     <center><button type="submit" name="button"
    class="btn btn-primary">
    <i class="glyphicon glyphicon-ok"></i>
      GUARDAR
  </button>
  <a href="<?php echo site_url('tecnicos/index'); ?>" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i>
CANCELAR
  </a></center>
  </div>

</div>
</form>
<!-- ...................07 -->
<script type="text/javascript">
     $("#frm_nuevo_tecnico").validate({
       rules:{
         fk_id_equ_vm:{
           required:true,
         },
         cedula_tec_vm:{
           required:true,
           minlength:10,
           maxlength:10,
           digits:true
         },
         apellido_tec_vm:{
           required:true,
           minlength:3


         },
         nombre_tec_vm:{
           required:true,
           minlength:3


         },
         telefono_tec_vm:{
           required:true,
           minlength:10,
           maxlength:10,
           digits:true

         },

         edad_tec_vm:{
           required:true,
         },
         email_tec_vm:{
           required:true,
           minlength:3

         },
         foto_tec_vm:{
           required:true,
         }
       },
       messages:{
         fk_id_equ_vm:{
           required:"Ingrese el dato",
         },
         cedula_tec_vm:{
            required:"Ingrese la cedula",
            minlength:"cedula incorrecta",
            maxlength:"cedula incorrecta",
            digits:"este campo solo acepta numeros"
         },
         apellido_tec_vm:{
            required:"Ingrese el Apellido",
             minlength:"Apellido incorrecto"
         },
         nombre_tec_vm:{
            required:"Ingrese el Nombre",
            minlength:"Nombre incorrecto"
         },
         telefono_tec_vm:{
         required:"Ingrese el numero de telefono",
         minlength:"Telefono incorrecto",
         maxlength:"Telefono incorrecto",
         digits:"Este campo solo acepta números"
         },

         edad_tec_vm:{
         required:"Ingrese la edad",
         },
         email_tec_vm:{
           required:"Ingrese el email",
           minlength:"email incorrecto"
         },
         foto_tec_vm:{
           required:"Ingrese la foto",
         }
       }

     });


</script>

<script type="text/javascript">
  $("#fk_id_equ_vm").val("<?php echo $tecnicoEditar->fk_id_equ_vm; ?>");
</script>
