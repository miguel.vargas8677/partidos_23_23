<legend class="text-center">
<i class= "glyphicon glyphicon-user"></i>Gestion de Tecnicos
</legend>
<hr>
<center>
  <a href="<?php echo site_url('tecnicos/nuevo'); ?>"
    class="btn btn-success"
    <i class="glyphicon glyphicon-plus glyphicon-circle"></i>
    AGREGAR TECNICOS
  </a>
</center>
<br>
<?php if ($listadoTecnicos): ?>
  <table id="tbl-tecnicos" class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">NOMBRE DEL EQUIPO</th>
        <th class="text-center">CEDULA</th>
        <th class="text-center">APELLIDO</th>
        <th class="text-center">NOMBRE</th>
        <th class="text-center">TELEFONO</th>
        <th class="text-center">EDAD</th>
        <th class="text-center">EMAIL</th>
        <th class="text-center">FOTO</th>
        <th class="text-center">ACCIONES</th>

      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoTecnicos->result() as $tecnicoTemporal): ?>
<tr>
  <td class="text-center"><?php echo $tecnicoTemporal->id_tec_vm; ?></td>
    <td class="text-center"><?php echo $tecnicoTemporal->nombre_equ_vm; ?></td>
  <td class="text-center"><?php echo $tecnicoTemporal->cedula_tec_vm; ?></td>
  <td class="text-center"><?php echo $tecnicoTemporal->apellido_tec_vm; ?></td>
  <td class="text-center"><?php echo $tecnicoTemporal->nombre_tec_vm; ?></td>
  <td class="text-center"><?php echo $tecnicoTemporal->telefono_tec_vm; ?></td>
  <td class="text-center"><?php echo $tecnicoTemporal->edad_tec_vm; ?></td>
  <td class="text-center"><?php echo $tecnicoTemporal->email_tec_vm; ?></td>
  <td class="text-center"><?php if ($tecnicoTemporal->foto_tec_vm!=""): ?>
                    <a href="<?php echo base_url('uploads/tecnicos').'/'.$tecnicoTemporal->foto_tec_vm; ?>"
                      target="_blank">
                      <img src="<?php echo base_url('uploads/tecnicos').'/'.$tecnicoTemporal->foto_tec_vm; ?>"
                      width="50px" height="50px"
                      alt="">
                    </a>
                  <?php else: ?>
                    N/A
                  <?php endif; ?>
                  </td>

<td class="text-center">
<a href="<?php echo site_url("tecnicos/editar"); ?>/<?php echo $tecnicoTemporal->id_tec_vm; ?>" class="btn btn-warning">
<i class="glyphicon glyphicon-edit"></i>
  EDITAR
</a>
<a href="<?php echo site_url("tecnicos/borrar"); ?>/<?php echo $tecnicoTemporal->id_tec_vm; ?>" class="btn btn-danger" onclick="return confirm('¿esta seguro de eliminar?');">
<i class="glyphicon glyphicon-trash"></i>
ELIMINAR
</a>
</tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php else: ?>
<h3><br>No existen tecnicos</br></h3>
<?php endif; ?>
<script type="text/javascript">
$("#tbl-tecnicos").DataTable();

</script>
