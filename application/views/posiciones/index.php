<legend class="text-center">
<i class= "glyphicon glyphicon-user"></i>Gestion de Posiciones
</legend>
<hr>
<center>
  <a href="<?php echo site_url('posiciones/nuevo'); ?>"
    class="btn btn-success"
    <i class="glyphicon glyphicon-plus glyphicon-circle"></i>
    AGREGAR POSICIONES
  </a>
</center>
<br>
<?php if ($listadoPosiciones): ?>
  <table id="tbl-posiciones" class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th class="text-center">ID</th>
        <th class="text-center">RESULTADO</th>
        <th class="text-center">PARTIDOS JUGADOS</th>
        <th class="text-center">PARTIDOS GANADOS</th>
        <th class="text-center">PARTIDOS PERDIDOS</th>
        <th class="text-center">PARTIDOS ENPATADOS</th>
        <th class="text-center">GOLES A FAVOR</th>
        <th class="text-center">GOLES EN CONTRA</th>
        <th class="text-center">GOLES EN DIFERENCIA</th>
        <th class="text-center">PUNTOS</th>
        <th class="text-center">ACCIONES</th>

      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoPosiciones->result() as $posicionTemporal): ?>
<tr>
  <td class="text-center"><?php echo $posicionTemporal->id_pos_vm; ?></td>
    <td class="text-center"><?php echo $posicionTemporal->resultado_faseg_vm; ?></td>
  <td class="text-center"><?php echo $posicionTemporal->partido_jugados_pos_vm; ?></td>
  <td class="text-center"><?php echo $posicionTemporal->partido_ganados_pos_vm; ?></td>
  <td class="text-center"><?php echo $posicionTemporal->partido_perdidos_pos_vm; ?></td>
  <td class="text-center"><?php echo $posicionTemporal->partido_enpatados_pos_vm; ?></td>
  <td class="text-center"><?php echo $posicionTemporal->goles_favor_pos_vm; ?></td>
  <td class="text-center"><?php echo $posicionTemporal->goles_contra_pos_vm; ?></td>
  <td class="text-center"><?php echo $posicionTemporal->goles_diferencia_pos_vm; ?></td>
  <td class="text-center"><?php echo $posicionTemporal->puntos_pos_vm; ?></td>


<td class="text-center">
<a href="<?php echo site_url("posiciones/editar"); ?>/<?php echo $posicionTemporal->id_pos_vm; ?>" class="btn btn-warning">
<i class="glyphicon glyphicon-edit"></i>
  EDITAR
</a>
<a href="<?php echo site_url("posiciones/borrar"); ?>/<?php echo $posicionTemporal->id_pos_vm; ?>" class="btn btn-danger" onclick="return confirm('¿esta seguro de eliminar?');">
<i class="glyphicon glyphicon-trash"></i>
ELIMINAR
</a>
</tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php else: ?>
<h3><br>No existen posiciones</br></h3>
<?php endif; ?>
<script type="text/javascript">
$("#tbl-posiciones").DataTable();

</script>
