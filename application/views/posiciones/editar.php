

<legend class="text-center">
<i class= "glyphicon glyphicon-plus"></i>Editar Posiciones
</legend>

<form id="frm_nuevo_posicion" class=""
enctype="multipart/form-data"
action="<?php echo site_url(); ?>/posiciones/procesarActualizacion"
 method="post">
<br>
<div class = "form-group">
  <div class="col-md-4 text-right">
    <center><input type="hidden"name="id_pos_vm" id=id_faseg_vm value="<?php echo $posicionEditar->id_pos_vm; ?>"></center>
    <br>
  <label for="">RESULTADO:</label>
    </div>
  <div class="col-sm-7">

<select class="form-control" name="fk_id_faseg_vm" id="fk_id_faseg_vm" required>
  <option value="">----Seleccione un Resultado-----</option>
  <?php if ($listadoGrupos): ?>
    <?php foreach ($listadoGrupos->result() as $grupoTemporal): ?>
      <option value="<?php echo $grupoTemporal->id_faseg_vm; ?>"><?php echo $grupoTemporal->resultado_faseg_vm; ?></option>
    <?php endforeach; ?>
  <?php endif; ?>
</select>
</div>
</div>
<br>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">PARTIDOS JUGADOS:</label>
  </div>
  <div class="col-md-7">
    <input type="number" id="partido_jugados_pos_vm" name="partido_jugados_pos_vm" value="<?php echo $posicionEditar->partido_jugados_pos_vm; ?>"
    class="form-control" placeholder="Ingrese el numero de partidos jugados" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">PARTIDOS GANADOS:</label>
  </div>
  <div class="col-md-7">
    <input type="number" id="partido_ganados_pos_vm" name="partido_ganados_pos_vm" value="<?php echo $posicionEditar->partido_ganados_pos_vm; ?>"
    class="form-control" placeholder="Ingrese el numero de partidos ganados" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">PARTIDOS PERDIDOS:</label>
  </div>
  <div class="col-md-7">
    <input type="number" id="partido_perdidos_pos_vm" name="partido_perdidos_pos_vm" value="<?php echo $posicionEditar->partido_perdidos_pos_vm; ?>"
    class="form-control" placeholder="Ingrese el numero de partidos perdidos" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">PARTIDOS ENPATADOS:</label>
  </div>
  <div class="col-md-7">
    <input type="number" id="partido_enpatados_pos_vm" name="partido_enpatados_pos_vm" value="<?php echo $posicionEditar->partido_enpatados_pos_vm; ?>"
    class="form-control" placeholder="Ingrese el numero de partidos enpatados" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">GOLES A FAVOR:</label>
  </div>
  <div class="col-md-7">
    <input type="number" id="goles_favor_pos_vm" name="goles_favor_pos_vm" value="<?php echo $posicionEditar->goles_favor_pos_vm; ?>"
    class="form-control" placeholder="Ingrese el numero de goles a favor" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">GOLES EN CONTRA:</label>
  </div>
  <div class="col-md-7">
    <input type="number" id="goles_contra_pos_vm" name="goles_contra_pos_vm" value="<?php echo $posicionEditar->goles_contra_pos_vm; ?>"
    class="form-control" placeholder="Ingrese el numero de goles en contra" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">GOLES EN DIFERENCIA:</label>
  </div>
  <div class="col-md-7">
    <input type="number" id="goles_diferencia_pos_vm" name="goles_diferencia_pos_vm" value="<?php echo $posicionEditar->goles_diferencia_pos_vm; ?>"
    class="form-control" placeholder="Ingrese el numero de goles en diferencia" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">PUNTOS:</label>
  </div>
  <div class="col-md-7">
    <input type="number" id="puntos_pos_vm" name="puntos_pos_vm" value="<?php echo $posicionEditar->puntos_pos_vm; ?>"
    class="form-control" placeholder="Ingrese el numero de puntos" >
  </div>
</div>
<br>
<div class="row">
  <div class="col-md-7">
     <center><button type="submit" name="button"
    class="btn btn-primary">
    <i class="glyphicon glyphicon-ok"></i>
      GUARDAR
  </button>
  <a href="<?php echo site_url('posiciones/index'); ?>" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i>
CANCELAR
  </a></center>
  </div>

</div>
</form>
<!-- ...................07 -->
<script type="text/javascript">
     $("#frm_nuevo_posicion").validate({
       rules:{
         fk_id_faseg_vm:{
           required:true,
         },
        partido_jugados_pos_vm:{
           required:true,

         },
         partido_ganados_pos_vm:{
           required:true,


         },
        partido_perdidos_pos_vm:{
           required:true,



         },
         partido_enpatados_pos_vm:{
           required:true,

         },

        goles_favor_pos_vm:{
           required:true,
         },
         goles_contra_pos_vm:{
           required:true,

         },
         goles_diferencia_pos_vm:{
           required:true,

         },
         puntos_pos_vm:{
           required:true,

         }

       },
       messages:{
         fk_id_faseg_vm:{
           required:"Ingrese el dato",
         },
         partido_jugados_pos_vm:{
            required:"Ingrese partidos jugados",

         },
         partido_ganados_pos_vm:{
            required:"Ingrese partidos ganados",

         },
         partido_perdidos_pos_vm:{
            required:"Ingrese partidos perdidos",

         },
         partido_enpatados_pos_vm:{
           required:"Ingrese partidos enpatados",

         },

         goles_favor_pos_vm:{
         required:"Ingrese goles a favor",
         },
         goles_contra_pos_vm:{
           required:"Ingrese goles en contra",

         },
         goles_diferencia_pos_vm:{
           required:"Ingrese goles a favor",

         },
         puntos_pos_vm:{
           required:"Ingrese los puntos",

         }

       }

     });


</script>

<script type="text/javascript">
  $("#fk_id_faseg_vm").val("<?php echo $posicionEditar->fk_id_faseg_vm; ?>");
</script>
