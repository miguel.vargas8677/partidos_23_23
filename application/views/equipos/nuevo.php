<legend class="text-center">

      <button type="button" class="btn btn-danger">
      <span class="glyphicon glyphicon-plus"></span>
      </button>
      Nuevo Equipo
      <br>
      <br>
</legend>

<form id="frm_nuevo_equipo" class=""
enctype="multipart/form-data"
 action="<?php echo site_url('equipos/guardarEquipo'); ?>" method="post">
  <div class="row">
    <div class="col-md-4 text-right">
      <label for="">NOMBRE: </label>
          </div>
          <div class="col-md-7">
            <input class="form-control" type="text" id="nombre_equ_vm" name="nombre_equ_vm" value="" placeholder="Por favor nombre del equipo" >

          </div>
  </div>

<br><br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">NOMBRE1: </label>
        </div>
        <div class="col-md-7">
          <input class="form-control" type="text" id="nombre1_equ_vm" name="nombre1_equ_vm" value="" placeholder="Por favor nombre del equipo" >

        </div>
</div>
<br>

  <div class="row">
    <div class="col-md-4 text-right">
      <label for="">ABREVIATURAS: </label>
          </div>
          <div class="col-md-7">
            <input class="form-control" type="text" id="abreviatura_equ_vm" name="abreviatura_equ_vm" value="" placeholder="Por favor Ingrese el Apellido" >
          </div>
  </div>

  <br>



      <div class="row">
        <div class="col-md-4 text-right">
          <label for="">FUNDACION: </label>
              </div>
              <div class="col-md-7">
                <input class="form-control" type="number" id="año_fundacion_equ_vm"  name="año_fundacion_equ_vm" value="" placeholder="Por favor Ingrese el Telefono"  >
              </div>
      </div>

      <br>
      <div class="row">
        <div class="col-md-4 text-right">
          <label for="">FOTO:</label>
        </div>
        <div class="col-md-7">
          <input type="file" id="foto_equ_vm" name="foto_equ_vm" value=""  accept="image/*" >
        </div>
      </div>
      <br>

          <div class="row">
            <div class="col-md-4">
                </div>
            <div class="col-md-7">
              <center><button type="submit" name="button"  class="btn btn-primary">
                <i class="fa fa-folder"></i>
                Guardar
              </button>
              <a href="<?php echo site_url(); ?>/equipos/index"
                    class="btn btn-warning">
                    <i class="fa fa-times"> </i>
                    CANCELAR
                  </a></center>
            </div>
          </div>
<br>
</form>

<script type="text/javascript">
     $("#frm_nuevo_equipo").validate({
        rules:{
            nombre_equ_vm:{
              required:true,
              minlength:3
            },

          abreviatura_equ_vm:{
              required:true,
              minlength:3
            },
            año_fundacion_equ_vm:{
              required:true,
              minlength:4,
              maxlength:4,
              digits:true
            },

            foto_equ_vm:{
              required:true
            }
        },
        messages:{
              nombre_equ_vm:{
              required:"Por favor ingrese el equipo",
              minlength:"Equipo Incorrecto"
            },

            abreviatura_equ:{
              required:"Por favor ingrese las abreviaturas del equipo",
              minlength:"Abreviatura incorrecto"
            },
          año_fundacion_equ_vm:{
              required:"Por favor ingrese el año en que se fundo el Equipo",
              minlength:"año incorrecto",
              maxlength:"año incorrecto",
              digits:"Este campo solo acepta números"
            },
            direccion_est:{
              required:"Por favor ingrese la dirección"
            },
            foto_equ_vm:{
              required:"Por favor seleccione la fotografia"
            }
        }
     });
</script>
