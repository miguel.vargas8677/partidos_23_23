<div class="row">
  <div class="col-md-12 text-center well">
    <h3>ACTUALIZAR EQUIPO</h3>
  </div>

</div>
<center>
  <a href="<?php echo site_url(); ?>/equipos/nuevo"
  class="btn btn-warning">
  <i class="fa fa-reply-all"> </i>
  Volver
</a>
</center>
<hr>

<div class="row">
  <div class="col-md-12">
<?php if ($equipoEditar): ?>
  <form id="frm_editar_equipo" class="" action="<?php echo site_url('equipos/procesarActualizacion'); ?>" method="post">

<center><input type="hidden" name="id_equ_vm" id="id_equ_vm" value="<?php echo $equipoEditar->id_equ_vm; ?>"></center>
        <div class="row">
        <div class="col-md-4 text-right">
          <label for="">NOMBRE: </label>
              </div>
              <div class="col-md-7">
                <input class="form-control" type="text" id="nombre_equ_vm" name="nombre_equ_vm" value="<?php echo $equipoEditar->nombre_equ_vm; ?>" placeholder="Por favor Ingrese el nombre del equipo"  >
              </div>
      </div>
      <br>

        <div class="row">
          <div class="col-md-4 text-right">
            <label for="">ABREVIATURAS: </label>
                </div>
                <div class="col-md-7">
                  <input class="form-control" type="text" id="abreviatura_equ_vm" name="abreviatura_equ_vm" value="<?php echo $equipoEditar->abreviatura_equ_vm; ?>" placeholder="Por favor Ingrese las abreviaturas"  >
                </div>
        </div>

        <br>

          <div class="row">
            <div class="col-md-4 text-right">
              <label for="">FUNDACION: </label>
                  </div>
                  <div class="col-md-7">
                    <input class="form-control" type="number" id="año_fundacion_equ_vm" name="año_fundacion_equ_vm" value="<?php echo $equipoEditar->año_fundacion_equ_vm; ?>" placeholder="Por favor Ingrese el año de fundacion del equipo" >
                  </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-4 text-right">
                  <label for="">FOTOGRAFIA</label>
                </div>
                <div class="col-md-7">
                      <input type="file" name="foto_est_vm"
                      accept="image/*"
                      id="foto_est_vm"  value="" >
                      </div>
                      <br>
                      <br>

              <br>
            <br>
            <div class="row">
              <div class="col-md-4">
                  </div>
              <div class="col-md-7">
                <center><button type="submit" name="button"  class="btn btn-primary">
                  <i class="fa fa-spinner"></i>
                  Actualizar
                </button>
                <a href="<?php echo site_url(); ?>/equipos/index"
                      class="btn btn-warning">
                      <i class="fa fa-times"> </i>
                      CANCELAR
                    </a></center>
              </div>
            </div>
  <br>
  </form>
<?php else: ?>
  <div class="alert alert-danger">
    <b>No se encontro estudiante </b>
  </div>
<?php endif; ?>
  </div>
</div>

<script type="text/javascript">
     $("#frm_editar_equipo").validate({
        rules:{
            nombre_equ_vm:{
              required:true,
              minlength:3
            },

          abreviatura_equ_vm:{
              required:true,
              minlength:3
            },
            año_fundacion_equ_vm:{
              required:true,
              minlength:4,
              maxlength:4,
              digits:true
            },

            foto_equ_vm:{
              required:true
            }
        },
        messages:{
              nombre_equ_vm:{
              required:"Por favor ingrese el equipo",
              minlength:"Equipo Incorrecto"
            },

            abreviatura_equ:{
              required:"Por favor ingrese las abreviaturas del equipo",
              minlength:"Abreviatura incorrecto"
            },
          año_fundacion_equ_vm:{
              required:"Por favor ingrese el año en que se fundo el Equipo",
              minlength:"año incorrecto",
              maxlength:"año incorrecto",
              digits:"Este campo solo acepta números"
            },
            direccion_est:{
              required:"Por favor ingrese la dirección"
            },
            foto_equ_vm:{
              required:"Por favor seleccione la fotografia"
            }
        }
     });
</script>
