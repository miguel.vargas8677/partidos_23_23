
<legend class="text-center">

      <button type="button" class="btn btn-danger">
        <span class="glyphicon glyphicon-user"></span>
      </button>

Final
<br>
<br>
<center>
    <a href="<?php echo site_url(); ?>/terminos/nuevo" class="btn btn-primary">
      <i class="fa fa-user-plus"></i>
      Agregar Nuevo
    </a>
  </center>
  <br>
  <br>
</legend>

<hr>

<?php if ($listadoTerminos): ?>
  <table id="tbl_terminos" class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th class="text-center">ID</th>
          <th class="text-center">FECHA DE PARTIDO</th>
          <th class="text-center">NOMBRE DEL EQUIPO</th>

      <th class="text-center">RESULTADO1</th>
        <th class="text-center">VS</th>
        <th class="text-center">NOMBRE DEL EQUIPO</th>
            <th class="text-center">RESULTADO2</th>
          <th class="text-center">ESTADIIO </th>
        <th class="text-center">ACCIONES</th>
    </tr>
  </thead>
  <tbody >
<?php foreach ($listadoTerminos->result()
 as $terminoTemporal): ?>
 <tr>
   <td class="text-center">
       <?php echo $terminoTemporal->id_fin_vm; ?>
  </td>
  <td class="text-center">
      <?php echo $terminoTemporal->fecha_partido_fin_vm ; ?>
 </td>


<td class="text-center"><?php echo $terminoTemporal->nombre_equ_vm; ?></td>

<td class="text-center">
    <?php echo $terminoTemporal->resultado_fin_vm; ?>
</td>
 <td class="text-center">
     <?php echo $terminoTemporal->vs_fin_vm ; ?>
</td>

<td class="text-center"><?php echo $terminoTemporal->nombre1_equ_vm; ?></td>
<td class="text-center">
    <?php echo $terminoTemporal->finalp_fin_vm; ?>
</td>
 <td class="text-center">
     <?php echo $terminoTemporal->nombre_est_vm; ?>
</td>



<td class="text-center">
  <a href="<?php echo site_url('terminos/editar'); ?>/<?php echo $terminoTemporal->id_fin_vm; ?>" class="btn btn-warning">
  <i class="fa fa-pencil"></i>
  Editar
    </a>
    <a href="<?php echo site_url('terminos/borrar'); ?>/<?php echo $terminoTemporal->id_fin_vm; ?>" class="btn btn-danger" onclick="return confirm('¿Esta seguro de eliminar?')">
    <i class="fa fa-trash"></i>
    Eliminar
    </a>


 </tr>

<?php endforeach; ?>

  </tbody>
  </table>
<?php else: ?>
<h3><b>No existe Partidos de final</b></h3>
<?php endif; ?>

<script type="text/javascript">
$("#tbl_terminos").DataTable();

</script>
