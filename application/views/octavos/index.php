
<legend class="text-center">

      <button type="button" class="btn btn-danger">
        <span class="glyphicon glyphicon-user"></span>
      </button>

Octavos de final
<br>
<br>
<center>
    <a href="<?php echo site_url(); ?>/octavos/nuevo" class="btn btn-primary">
      <i class="fa fa-user-plus"></i>
      Agregar Nuevo
    </a>
  </center>
  <br>
  <br>
</legend>

<hr>

<?php if ($listadoOctavos): ?>
  <table id="tbl_octavos" class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th class="text-center">ID</th>
          <th class="text-center">FECHA DE PARTIDO</th>
          <th class="text-center">NOMBRE DEL EQUIPO</th>

      <th class="text-center">RESULTADO1</th>
        <th class="text-center">VS</th>
        <th class="text-center">NOMBRE DEL EQUIPO</th>
            <th class="text-center">RESULTADO2</th>
          <th class="text-center">ESTADIIO </th>
        <th class="text-center">ACCIONES</th>
    </tr>
  </thead>
  <tbody >
<?php foreach ($listadoOctavos->result()
 as $octavoTemporal): ?>
 <tr>
   <td class="text-center">
       <?php echo $octavoTemporal->id_oct_vm; ?>
  </td>
  <td class="text-center">
      <?php echo $octavoTemporal->fecha_partido_oct_vm ; ?>
 </td>


<td class="text-center"><?php echo $octavoTemporal->nombre_equ_vm; ?></td>

<td class="text-center">
    <?php echo $octavoTemporal->resultado_oct_vm; ?>
</td>
 <td class="text-center">
     <?php echo $octavoTemporal->vs_oct_vm ; ?>
</td>

<td class="text-center"><?php echo $octavoTemporal->nombre1_equ_vm; ?></td>
<td class="text-center">
    <?php echo $octavoTemporal->finalp_oct_vm; ?>
</td>
 <td class="text-center">
     <?php echo $octavoTemporal->nombre_est_vm; ?>
</td>



<td class="text-center">
  <a href="<?php echo site_url('octavos/editar'); ?>/<?php echo $octavoTemporal->id_oct_vm; ?>" class="btn btn-warning">
  <i class="fa fa-pencil"></i>
  Editar
    </a>
    <a href="<?php echo site_url('octavos/borrar'); ?>/<?php echo $octavoTemporal->id_oct_vm; ?>" class="btn btn-danger" onclick="return confirm('¿Esta seguro de eliminar?')">
    <i class="fa fa-trash"></i>
    Eliminar
    </a>


 </tr>

<?php endforeach; ?>

  </tbody>
  </table>
<?php else: ?>
<h3><b>No existe Partidos de Octavos de final</b></h3>
<?php endif; ?>

<script type="text/javascript">
$("#tbl_octavos").DataTable();

</script>

    <!-- Results Section Begin -->
    <section class="schedule-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 left-blog-pad">
                    <div class="schedule-text">
                        <h4 class="st-title">CATAR 2022</h4>
                        <div class="st-table">
                            <table>
                                <tbody>
                                    <tr>
                                        <td class="left-team">
                                            <img src="https://media.istockphoto.com/id/1148998858/es/vector/bandera-de-ecuador.jpg?s=612x612&w=0&k=20&c=sUS5DA6ipFQD0AmtxJ_6w1zqnm3CgrWyZIuZsaCp3Bw=" alt="">
                                            <h4>Ecuador</h4>
                                        </td>
                                        <td class="st-option">
                                            <div class="so-text">Estadio, Estadio Al Janoub</div>
                                            <h4>2 : 0</h4>
                                            <div class="so-text">15 Septiembre 2022</div>
                                        </td>
                                        <td class="right-team">
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/Flag_of_Qatar.svg/2560px-Flag_of_Qatar.svg.png">
                                            <h4>Qatar</h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="left-team">
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/8/88/Flag_of_Australia_%28converted%29.svg">
                                            <h4>Australia</h4>
                                        </td>
                                        <td class="st-option">
                                            <div class="so-text">Estadio,Estadio Lusail</div>
                                            <h4>0 : 1</h4>
                                            <div class="so-text">15 Septiembre 2022</div>
                                        </td>
                                        <td class="right-team">
                                            <img src="<?php echo base_url(); ?>/assets/img/schedule/flag-4.jpg" alt="">
                                            <h4>France</h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="left-team">
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Flag_of_Germany.svg/640px-Flag_of_Germany.svg.png">
                                            <h4>Alemania</h4>
                                        </td>
                                        <td class="st-option">
                                            <div class="so-text">Estadio,Estadio 974</div>
                                            <h4>2 : 2</h4>
                                            <div class="so-text">15 Septiembre 2022</div>
                                        </td>
                                        <td class="right-team">
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/Bandera_de_Espa%C3%B1a.svg/1200px-Bandera_de_Espa%C3%B1a.svg.png">
                                            <h4>España</h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="left-team">
                                            <img src="<?php echo base_url(); ?>/assets/img/schedule/flag-7.jpg" alt="">
                                            <h4>Croacia</h4>
                                        </td>
                                        <td class="st-option">
                                            <div class="so-text">Estadio,Estadio Al Bayt</div>
                                            <h4>3 : 1</h4>
                                            <div class="so-text">15 Septiembre 2022</div>
                                        </td>
                                        <td class="right-team">
                                            <img src="<?php echo base_url(); ?>/assets/img/schedule/flag-8.jpg" alt="">
                                            <h4>Uruguay</h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="left-team">
                                            <img src="<?php echo base_url(); ?>/assets/img/schedule/flag-9.jpg" alt="">
                                            <h4>Brazil</h4>
                                        </td>
                                        <td class="st-option">
                                            <div class="so-text">Estadio,
Estadio Al Janoub</div>
                                            <h4>1 : 3</h4>
                                            <div class="so-text">15 Septiembre 2022</div>
                                        </td>
                                        <td class="right-team">
                                            <img src="https://media.istockphoto.com/id/472317387/es/vector/bandera-de-suecia.jpg?s=612x612&w=0&k=20&c=ygd2DSDs7z6Jc9aaoDvP1Mgtg8p65OpAXBaq6Rlll4o=">
                                            <h4>Suezia</h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="left-team">
                                            <img src="https://img.asmedia.epimg.net/resizer/1e2w2reCQC2kUSn-B_Q5_Jp8Ok8=/360x203/cloudfront-eu-central-1.images.arcpublishing.com/diarioas/HT4TFMW3CFCTXBQVM22XCMNF2Q.jpg">
                                            <h4>Inglaterra</h4>
                                        </td>
                                        <td class="st-option">
                                            <div class="so-text">Estadio, Estadio Ahmad Bin Ali</div>
                                            <h4>2 : 3</h4>
                                            <div class="so-text">15 Septiembre 2022</div>
                                        </td>
                                        <td class="right-team">
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Flag_of_the_United_States.svg/1200px-Flag_of_the_United_States.svg.png">
                                            <h4>Estados Unidos</h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="left-team">
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Flag_of_Morocco.svg/200px-Flag_of_Morocco.svg.png">
                                            <h4>Marruecos</h4>
                                        </td>
                                        <td class="st-option">
                                            <div class="so-text">Estadio, Estadio Al Thumama</div>
                                            <h4>1 : 0</h4>
                                            <div class="so-text">15 Septiembre 2022</div>
                                        </td>
                                        <td class="right-team">
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Flag_of_Canada_%28Pantone%29.svg/800px-Flag_of_Canada_%28Pantone%29.svg.png">
                                            <h4>Canada</h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="left-team">
                                            <img src="<?php echo base_url(); ?>/assets/img/schedule/flag-14.jpg" alt="">
                                            <h4>Argentina</h4>
                                        </td>
                                        <td class="st-option">
                                            <div class="so-text">Estadio, Estadio Ahmad Bin Ali</div>
                                            <h4>0 : 0</h4>
                                            <div class="so-text">15 Septiembre 2022</div>
                                        </td>
                                        <td class="right-team">
                                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Bandera_de_M%C3%A9xico_%281914-1920%29.svg/1200px-Bandera_de_M%C3%A9xico_%281914-1920%29.svg.png">
                                            <h4>Mexico</h4>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </section>
