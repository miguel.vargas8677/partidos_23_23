
<legend class="text-center">

      <button type="button" class="btn btn-danger">
        <span class="glyphicon glyphicon-user"></span>
      </button>

Gestion de Estadios
<br>
<br>
<center>
    <a href="<?php echo site_url(); ?>/estadios/nuevo" class="btn btn-primary">
      <i class="fa fa-user-plus"></i>
      Agregar Nuevo
    </a>
  </center>
  <br>
  <br>
</legend>

<hr>

<?php if ($listadoEstadios): ?>
  <table id="tbl_estadios" class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th class="text-center">ID</th>
          <th class="text-center">NOMBRE</th>
          <th class="text-center">PAIS</th>
        <th class="text-center">CIUDAD</th>
      <th class="text-center">CAPACIDAD</th>
          <th class="text-center">FOTOGRAFIA</th>
        <th class="text-center">ACCIONES</th>
    </tr>
  </thead>
  <tbody >
<?php foreach ($listadoEstadios->result()
 as $estadioTemporal): ?>
 <tr>
   <td class="text-center">
       <?php echo $estadioTemporal->id_est_vm; ?>
  </td>
   <td class="text-center">
       <?php echo $estadioTemporal->nombre_est_vm; ?>
  </td>
  <td class="text-center">
      <?php echo $estadioTemporal->pais_est_vm; ?>
 </td>
  <td class="text-center">
      <?php echo $estadioTemporal->ciudad_est_vm ; ?>
 </td>
  <td class="text-center">
      <?php echo $estadioTemporal->capacidad_est_vm; ?>
 </td>


    <td class="text-center">
                                      <?php if ($estadioTemporal->	foto_est_vm!=""): ?>
                                        <a href="<?php echo base_url(); ?>/uploads/estadios/<?php echo $estadioTemporal->	foto_est_vm; ?>"
                                          target="_blank">
                                        <img class="img-circle" src="<?php echo base_url(); ?>/uploads/estadios/<?php echo $estadioTemporal->	foto_est_vm; ?>"
                                        height="80px"
                                        width="100px"
                                        alt="">
                                        </a>
                                      <?php else: ?>
                                        N/A
                                      <?php endif; ?>
                                    </td>


<td class="text-center">
  <a href="<?php echo site_url('estadios/editar'); ?>/<?php echo $estadioTemporal->id_est_vm; ?>" class="btn btn-warning">
  <i class="fa fa-pencil"></i>
  Editar
    </a>
    <a href="<?php echo site_url('estadios/borrar'); ?>/<?php echo $estadioTemporal->id_est_vm; ?>" class="btn btn-danger" onclick="return confirm('¿Esta seguro de eliminar?')">
    <i class="fa fa-trash"></i>
    Eliminar
    </a>


 </tr>

<?php endforeach; ?>

  </tbody>
  </table>
<?php else: ?>
<h3><b>No existe Estadios</b></h3>
<?php endif; ?>

<script type="text/javascript">
$("#tbl_estadios").DataTable();

</script>

    <!-- Results Section Begin -->
    <section class="schedule-section spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 left-blog-pad">
                    <div class="schedule-text">
                        <h4 class="st-title">ESTADIOS DE CATAR 2022</h4>
                        <div class="st-table">
                            <table>
                                <tbody>
                                    <tr>

                                  <center>  <div class="row">
                   <div class="col-lg-12">
                       <div class="cc-pic">
                           <img src="https://www.qatar2022.qa/book/media/bvubliur/al_bayt_stadium.jpg" alt="">


  <h4>Estadio Al Bayt</h4>
</td><br><hr></center>

<center><div class="cc-pic">
    <img src="https://www.qatar2022.qa/book/media/tqqjpgav/al_janoub_stadium.jpg" alt="">
<h4>Estadio Al Janoub</h4>
</td>
</div>
</td><br><hr></center>

                 <center><div class="cc-pic">
                     <img src="https://www.qatar2022.qa/book/media/dornt4rg/ahmad_bin_ali_stadium.jpg" alt="">
                 <h4>Estadio Ahmad Bin Ali</h4>
                 </td></div>
               </td><br><hr></center>


              <center><div class="cc-pic">
            <img src="https://www.qatar2022.qa/book/media/bt2ebwn0/al_thumama_stadium.jpg" alt="">
          <h4>Estadio Al Thumama</h4>
          </td></div>
        </td><br><hr></center>

          <center> <div class="cc-pic">
       <img src="https://www.qatar2022.qa/book/media/ma5lbv1x/education_city_stadium.jpg" alt="">
       <h4>Estadio Education City</h4>
     </td></div>
   </td><br><hr></center>

    <center>  <div class="cc-pic">
      <img src="https://www.qatar2022.qa/book/media/ck5poako/khalifa_stadium.jpg" alt="">
    <h4>Estadio Internacional Khalifa</h4>
  </td></div>
</td><br><hr></center>

    <center> <div class="cc-pic">
     <img src="https://www.qatar2022.qa/book/media/pi1ba5fp/lusail_stadium.jpg" alt="">
     <h4>Estadio Lusail</h4>
 </td></div>
</td><br><hr></center>

      <center><div class="cc-pic">
      <img src="https://www.qatar2022.qa/book/media/mqxh2swn/stadium_974.jpg" alt="">
<h4>Estadio 974</h4>
    </td></div>
   <br></tr></center>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
