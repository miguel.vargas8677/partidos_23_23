<legend class="text-center">

      <button type="button" class="btn btn-danger">
      <span class="glyphicon glyphicon-plus"></span>
      </button>
      Nuevo Estadio
      <br>
      <br>
</legend>

<form id="frm_nuevo_estadio" class=""
enctype="multipart/form-data"
 action="<?php echo site_url('estadios/guardarEstadio'); ?>" method="post">
  <div class="row">
    <div class="col-md-4 text-right">
      <label for="">NOMBRE: </label>
          </div>
          <div class="col-md-7">
            <input class="form-control" type="text" id="nombre_est_vm" name="nombre_est_vm" value="" placeholder="Por favor nombre del estadio" >

          </div>
  </div>

<br><br>
<div class="row">
  <div class="col-md-4 text-right">
    <label for="">PAIS: </label>
        </div>
        <div class="col-md-7">
          <input class="form-control" type="text" id="pais_est_vm" name="pais_est_vm" value="" placeholder="Por favor ingrese el pais" >

        </div>
</div>
<br>

  <div class="row">
    <div class="col-md-4 text-right">
      <label for="">CIUDAD: </label>
          </div>
          <div class="col-md-7">
            <input class="form-control" type="text" id="ciudad_est_vm" name="ciudad_est_vm" value="" placeholder="Por favor Ingrese la ciudad" >
          </div>
  </div>

  <br>



      <div class="row">
        <div class="col-md-4 text-right">
          <label for="">CAPACIDAD: </label>
              </div>
              <div class="col-md-7">
                <input class="form-control" type="number" id="capacidad_est_vm"  name="capacidad_est_vm" value="" placeholder="Por favor Ingrese la capacidad"  >
              </div>
      </div>

      <br>
      <div class="row">
        <div class="col-md-4 text-right">
          <label for="">FOTO:</label>
        </div>
        <div class="col-md-7">
          <input type="file" id="foto_est_vm" name="foto_est_vm" value=""  accept="image/*" >
        </div>
      </div>
      <br>

          <div class="row">
            <div class="col-md-4">
                </div>
            <div class="col-md-7">
              <center><button type="submit" name="button"  class="btn btn-primary">
                <i class="fa fa-folder"></i>
                Guardar
              </button>
              <a href="<?php echo site_url(); ?>/estadios/index"
                    class="btn btn-warning">
                    <i class="fa fa-times"> </i>
                    CANCELAR
                  </a></center>
            </div>
          </div>
<br>
</form>


<script type="text/javascript">
     $("#frm_nuevo_estadio").validate({
        rules:{

            nombre_est_vm:{
              required:true,
              minlength:3
            },
            pais_est_vm:{
              required:true,
              minlength:3
            },
            ciudad_est_vm:{
              required:true,
              minlength:3
            },
            capacidad_est_vm:{
              required:true,
              minlength:5,
              maxlength:5,
              digits:true
            },
            direccion_est:{
              required:true
            },
            foto_est_vm:{
              required:true
            }

        },
        messages:{

            nombre_est_vm:{
              required:"Por favor ingrese El nombre del estadio",
              minlength:"Nombre incorrecto"
            },
            pais_est_vm:{
              required:"Por favor ingrese El Pais",
              minlength:"Pais incorrecto"
            },
            ciudad_est_vm:{
              required:"Por favor ingrese la ciudad",
              minlength:"Ciudad incorrecta"
            },

              capacidad_est_vm:{
              required:"Por favor ingrese la capacidad del estadio"
            },
            foto_est_vm:{
              required:"Por favor seleccione la fotografia"
            }

        }
     });
</script>
