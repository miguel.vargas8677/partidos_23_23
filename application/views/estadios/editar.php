<div class="row">
  <div class="col-md-12 text-center well">
    <h3>ACTUALIZAR ESTADIOS</h3>
  </div>

</div>
<center>
  <a href="<?php echo site_url(); ?>/estadios/nuevo"
  class="btn btn-warning">
  <i class="fa fa-reply-all"> </i>
  Volver
</a>
</center>
<hr>

<div class="row">
  <div class="col-md-12">
<?php if ($estadioEditar): ?>
  <form id="frm_editar_estadio" class="" action="<?php echo site_url('estadios/procesarActualizacion'); ?>" method="post">

<center><input type="hidden" name="id_est_vm" id="id_est_vm" value="<?php echo $estadioEditar->id_est_vm; ?>"></center>
        <div class="row">
        <div class="col-md-4 text-right">
          <label for="">NOMBRE: </label>
              </div>
              <div class="col-md-7">
                <input class="form-control" type="text" id="nombre_est_vm" name="nombre_est_vm" value="<?php echo $estadioEditar->nombre_est_vm; ?>" placeholder="Por favor Ingrese el nombre del estadio"  >
              </div>
      </div>
      <br>

        <div class="row">
          <div class="col-md-4 text-right">
            <label for="">PAIS: </label>
                </div>
                <div class="col-md-7">
                  <input class="form-control" type="text" id="pais_est_vm" name="pais_est_vm" value="<?php echo $estadioEditar->pais_est_vm; ?>" placeholder="Por favor Ingrese el pais"  >
                </div>
        </div>

        <br>

          <div class="row">
            <div class="col-md-4 text-right">
              <label for="">CIUDAD: </label>
                  </div>
                  <div class="col-md-7">
                    <input class="form-control" type="text" id="ciudad_est_vm" name="ciudad_est_vm" value="<?php echo $estadioEditar->ciudad_est_vm; ?>" placeholder="Por favor Ingrese el año de fundacion la ciudad" >
                  </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-4 text-right">
              <label for="">CAPACIDAD: </label>
                  </div>
                  <div class="col-md-7">
                    <input class="form-control" type="number" id="capacidad_est_vm" name="capacidad_est_vm" value="<?php echo $estadioEditar->capacidad_est_vm; ?>" placeholder="Por favor Ingrese la capacidad" >
                  </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-4 text-right">
                  <label for="">FOTOGRAFIA</label>
                </div>
                <div class="col-md-7">
                      <input type="file" name="foto_est_vm"
                      accept="image/*"
                      id="foto_est_vm"  value="" >
                      </div>
                      <br>
                      <br>

              <br>
            <br>

              <div class="col-md-12">
                <center><button type="submit" name="button"  class="btn btn-primary">
                  <i class="fa fa-spinner"></i>
                  Actualizar
                </button>
                <a href="<?php echo site_url(); ?>/estadios/index"
                      class="btn btn-warning">
                      <i class="fa fa-times"> </i>
                      CANCELAR
                    </a></center>
              </div>
            </div>
  <br>
  </form>
<?php else: ?>
  <div class="alert alert-danger">
    <b>No se encontro estudiante </b>
  </div>
<?php endif; ?>
  </div>
</div>

<script type="text/javascript">
     $("#frm_nuevo_estadio").validate({
        rules:{

            nombre_est_vm:{
              required:true,
              minlength:3
            },
            pais_est_vm:{
              required:true,
              minlength:3
            },
            ciudad_est_vm:{
              required:true,
              minlength:3
            },
            capacidad_est_vm:{
              required:true,
              minlength:5,
              maxlength:5,
              digits:true
            },
            direccion_est:{
              required:true
            },
            foto_est_vm:{
              required:true
            }

        },
        messages:{

            nombre_est_vm:{
              required:"Por favor ingrese El nombre del estadio",
              minlength:"Nombre incorrecto"
            },
            pais_est_vm:{
              required:"Por favor ingrese El Pais",
              minlength:"Pais incorrecto"
            },
            ciudad_est_vm:{
              required:"Por favor ingrese la ciudad",
              minlength:"Ciudad incorrecta"
            },

              capacidad_est_vm:{
              required:"Por favor ingrese la capacidad del estadio"
            },
            foto_est_vm:{
              required:"Por favor seleccione la fotografia"
            }

        }
     });
</script>
